<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSenderUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senderU', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('userName')->unique();
            $table->string('F_name');
            $table->string('L_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->date('Birthday');
            $table->bigInteger('ambulance_id')->unsigned();
            $table->rememberToken();

            $table->foreign('ambulance_id')->references('id')->on('ambulances')->onDelete('restrict');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sender_users');
    }
}
