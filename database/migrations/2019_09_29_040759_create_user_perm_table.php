<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_perms', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned();
            $table->string('perName');
            $table->foreign('user_id')->references('id')->on('users');
            $table->primary(['user_id','perName']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_perms');
    }
}
