<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });


        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->timestamps();
        });

        Schema::create('roles_permissions', function (Blueprint $table) {
            $table->bigIncrements('role_id')->unsigned();
            $table->bigIncrements('permission_id')->unsigned();
            $table->primary(['role_id','permission_id']);

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('restrict');
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('restrict');

//            $table->timestamps();
        });

        Schema::create('users_roles', function (Blueprint $table) {
            $table->bigIncrements('users_id')->unsigned();
            $table->bigIncrements('roles_id')->unsigned();
            $table->primary(['users_id','roles_id']);

            $table->foreign('users_id')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('roles_id')->references('id')->on('roles')->onDelete('restrict');

            $table->timestamps();
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('roles_permissions');
        Schema::dropIfExists('users_roles');
    }
}
