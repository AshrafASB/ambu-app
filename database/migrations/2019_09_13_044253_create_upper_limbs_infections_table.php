<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpperLimbsInfectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upper_limbs_infections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',100);
            $table->bigInteger('upper_limbs_id')->unsigned();
            $table->timestamps();

            $table->foreign('upper_limbs_id')->references('id')->on('bodies')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upper_limbs_infections');

    }
}

///restrict ---> لا تحدف يوزر له ابناء
//cascade ---> احدف اليوزر مع جميع ابنائه سواء بوست له او صور او اي شي /}
