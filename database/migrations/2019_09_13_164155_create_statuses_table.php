<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->enum('Gender',['Male','Female']);
            $table->boolean('arrived');


            $table->bigInteger('age_id')->unsigned();//FK
            $table->bigInteger('sender_id')->unsigned();//FK
            $table->bigInteger('ambulance_id')->unsigned();//FK

            //body
            $table->text('Body');

            //bag
            $table->text('Bag')->nullable();


            $table->timestamps();

            $table->foreign('age_id')->references('id')->on('agetypes')->onDelete('restrict');
            $table->foreign('sender_id')->references('id')->on('senderU')->onDelete('restrict');
            $table->foreign('ambulance_id')->references('id')->on('ambulances')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
