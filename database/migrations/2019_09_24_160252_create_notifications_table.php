<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');         // store the class of notification

            $table->morphs('notifiable');    // it create to table notifiable_Id & notifiable_type
                                                   // it store in notifiable_type => the class of user if i have multible of user
                                                   // notifiable it the real id of user

            $table->text('data');

            $table->timestamp('read_at')->nullable(); // to know if user read this notify or nat
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
