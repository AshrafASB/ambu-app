<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class injection_category extends Model
{
    
    protected $table = 'injection_categories';
    protected $fillable = [
        'name','injection_id',
    ];
    
}
