<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class influence_category extends Model
{
    
    protected $table = 'influence_categories';
    protected $fillable = [
        'name','influence_id',
    ];
}
