<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chest_abdomen_infection extends Model
{
    
    protected $table = 'chest_abdomen_infections';
    protected $fillable = [
        'name','chest_abdomen_id',
    ];

}
