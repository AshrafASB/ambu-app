<?php

namespace App\Notifications;

use \App\senderUser;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Prophecy\Exception\Doubler\MethodNotFoundException;
use Illuminate\Http\Request;


class statusNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $user;
    public function __construct(senderUser $user=null)
    {

        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

  
    public function toDatabase($notifiable)
    {
        return [
            'icon' => 'like',
            'message' => sprintf('%s send data to you ', $this->user->F_name),
            'user_id' => $this->user->id,
            'url' => route('status.done')


        ];
    }

    public function toArray($notifiable)
    {
        return [
            'icon' => 'like',
            // 'message' => sprintf('%s send data to you ', $this->user->F_name),
            // هاي بقصد فيها اليوزر الي بعتلك الاشعار كالتالي
            // 'user_id' => $this->user->id,
            // 'url' => route('status.done')


        ];
    }
}
