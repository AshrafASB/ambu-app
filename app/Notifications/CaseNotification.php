<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CaseNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
//      return ['mail','broadcast','database','nexmo'];
        return ['broadcast','database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }


    public function toBroadcast($notifiable){
        return new BroadcastMessage([
            'url'=>route('#'),
            //id Status,
            'message'=>sprintf('%s New case from Ambulance %s')


        ]);
        // ممكن استخدم الابوجيكت و ممكن لا
        // الميزة تكمن في التالي لو بدي ابعت الاشعارات على شكل طابور و على اي كونكشن حتشتغل
        // فهي تسهل علينا العمل اما بدونها لا استطيع استخدام الطابور

        /*الطابور له شغلتين اسم و كونكشن (اي سيرفيس الي حستخدمها معه)*/
    }
        // write the varible her
        // write the url

    public function toDatabase($notifiable){
//        return [
//            'url'=> route(''),
//            'message' => sprintf('%s New case from Ambulance %s')
//        ];

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
