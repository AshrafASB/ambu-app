<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Sender extends Authenticatable
{
    use HasApiTokens,Notifiable;
    
    protected $table = 'senderu';
    protected $fillable = [
        'F_name','L_name','username', 'email', 'Birthday' , 'password','ambulance_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['birthday'];


    public function Ambulance(){
        return $this->belongsTo(Ambulance::class,'ambulance_id','id')->withDefault([
            'id' => '0',
            'name' => 'Uncategorized',
        ]);;
    
        }

    
}
