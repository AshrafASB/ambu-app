<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\Notifications\CaseNotification;

class senderUser extends Authenticatable
{
    use HasApiTokens,Notifiable;


//    $user->notify(new InvoicePaid($invoice));


    protected $table = 'senderu';
    protected $fillable = [
        'id','F_name', 'L_name', 'username', 'email', 'Birthday', 'password', 'ambulance_id',
    ];

    /**حاح
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['birthday'];


    public function Ambulance()
    {
        return $this->belongsTo(ambulance::class, 'ambulance_id', 'id')->withDefault([
            'id' => '0',
            'name' => 'Uncategorized',
        ]);;
    }



}
