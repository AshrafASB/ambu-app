<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ambulance extends Model
{
    
    protected $table = 'ambulances';
    protected $fillable = [
        'name',
    ];


    public function senders(){
    return $this->hasMany(User::class,'ambulance_id','id');

    }

}
