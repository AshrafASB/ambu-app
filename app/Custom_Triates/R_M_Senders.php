<?php

namespace App\Custom_Triates;   

trait R_M_Senders
{
    
    
    private function rules($id = null)
    {
        $rules =  [
            'firstName'  => 'required|max:255|min:3',
            'lastName'  => 'required|max:255|min:3',
            'Birthdate'  => 'required',
            'email'  => 'required|email|unique:users',
            'password'  =>  'required|min:3',

            'username'  => 'required|max:255|min:3|unique:users',
            'Ambulance'  => 'required',
    
    ];

       
    if($id)
    {

        $rules['password'] = '';
    }else{
    
        $rules['password'] =  'required|min:3';
        
    }

            return $rules;
        }


    private function messages()
    {
        return [
            'firstName.required' => 'Please enter your first name',
            'username.required' => 'Please enter your username',
            'firstName.min' => 'firstName length is too short',
            'firstName.max' => 'firstName length Max 255 char',
           
            'lastName.required' => 'Please enter your last name',
            'lastName.min' => 'lastName length is too short',
            'lastName.max' => 'lastName length Max 255 char',
            
            'Birthdate.required' => 'Birthdate is required',

            'email.required' => 'Please enter your email',
            'email.email' => 'enter a valid email',
            
            'password.required' => 'Please enter your password',
            'password.min' => 'password length is too short',

        ];
    }

}
