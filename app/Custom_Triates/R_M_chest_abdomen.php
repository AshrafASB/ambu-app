<?php

namespace App\Custom_Triates;

trait R_M_chest_abdomen
{


    private function rules()
    {
        $rules =  [
            'name'  => 'required|max:255|min:0',
            'chest_abdomen'  => 'required',
            
        ];
        return $rules;
    }
    private function messages()
    {
        return [
            'name.required' => 'Please enter a valid name',            
            'chest_abdomen.required' => 'Please choose body category',
            'name.min' => 'name length is too short',
            'name.max' => 'name length Max 255 char',
        ];
    }
}
