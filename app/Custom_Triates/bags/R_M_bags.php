<?php

namespace App\Custom_Triates\bags;

trait R_M_bags
{


    private function rules()
    {
        $rules =  [
            'name'  => 'required|max:255|min:0',
        ];
        return $rules;
    }
    private function messages()
    {
        return [
            'name.required' => 'Please enter a valid name',
            'name.min' => 'name length is too short',
            'name.max' => 'name length Max 255 char',
        ];
    }
}
