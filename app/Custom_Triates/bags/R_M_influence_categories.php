<?php

namespace App\Custom_Triates\bags;

trait R_M_influence_categories
{


    private function rules()
    {
        $rules =  [
            'name'  => 'required|max:255|min:0',
            'influence_categorie_id'  => 'required',
            
        ];
        return $rules;
    }
    private function messages()
    {
        return [
            'name.required' => 'Please enter a valid name',            
            'influence_categorie_id.required' => 'Please choose bag category',
            'name.min' => 'name length is too short',
            'name.max' => 'name length Max 255 char',
        ];
    }
}
