<?php
namespace App\Custom_Triates\bags;

trait R_M_injection_categories
{


    private function rules()
    {
        $rules =  [
            'name'  => 'required|max:255|min:0',
            'injection_id'  => 'required',
            
        ];
        return $rules;
    }
    private function messages()
    {
        return [
            
            'name.required' => 'Please enter a valid name',            
            'injection_id.required' => 'Please choose bag category',
            
            'name.min' => 'name length is too short',
            'name.max' => 'name length Max 255 char',
        ];
    }
}
