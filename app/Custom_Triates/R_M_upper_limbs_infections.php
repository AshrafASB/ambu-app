<?php

namespace App\Custom_Triates;

trait R_M_upper_limbs_infections
{


    private function rules()
    {
        $rules =  [
            'name'  => 'required|max:255|min:0',
            'upper_limbs_infections_id'  => 'required',
            
        ];
        return $rules;
    }
    private function messages()
    {
        return [
            'name.required' => 'Please enter a valid name',            
            'upper_limbs_infections_id.required' => 'Please choose body category',
           'name.min' => 'name length is too short',
            'name.max' => 'name length Max 255 char',
        ];
    }
}
