<?php

namespace App\Custom_Triates;   

trait R_M_Receptions
{
    
    
    private function rules($id = null)
    {
        $rules =  [
            'firstName'  => 'required|max:255|min:3',
            'lastName'  => 'required|max:255|min:3',
            'Birthdate'  => 'required',
            // 'email'  => 'required|email|unique:users',
            // 'password'  =>  'required|min:3',

            // 'username'  => 'required|max:255|min:3',
    
    ];

       
    if($id)
    {

        $rules['email'] = 'required|unique:users,email,'. $id;
        $rules['username'] = 'required|max:255|min:3|unique:users,username,'. $id;
    }else{
        // هنا بكون بعمل انشاء يعني لازم افحص كل الداتابيز بدون استثناء كالتالي
        // وكذلك الصورة مش ضروري يرفع لك صورة ف فورم التعديل ممكن ما بدو يعدل عليها ف بخلي السابقة 
        $rules['email'] = 'required|unique:users,email,';
        $rules['username'] = 'required|max:255|min:3|unique:users,username,';

    }
            return $rules;
        }


    private function messages()
    {
        return [
            'firstName.required' => 'Please enter your first name',
            'username.required' => 'Please enter your username',
            'firstName.min' => 'firstName length is too short',
            'firstName.max' => 'firstName length Max 255 char',
           
            'lastName.required' => 'Please enter your last name',
            'lastName.min' => 'lastName length is too short',
            'lastName.max' => 'lastName length Max 255 char',
            
            'Birthdate.required' => 'Birthdate is required',

            'email.required' => 'Please enter your email',
            'email.email' => 'enter a valid email',
            
            'password.required' => 'Please enter your password',
            'password.min' => 'password length is too short',

        ];
    }

}
