<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // هذه الرول مين اليوزرز الي ماخدينها
    //M2M (users & roles)
    public function users(){
        return $this->belongsToMany(User::class, 'users_roles');
    }
    // الرول الها بيرميشن
    public function permissions(){
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }
}