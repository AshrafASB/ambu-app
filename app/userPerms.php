<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userPerms extends Model
{
    protected $table='user_perms';
    public $incrementing = false;
    public $timestamps = false;
}
