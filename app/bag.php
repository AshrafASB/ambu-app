<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bag extends Model
{
    protected $table = 'bags';
    protected $fillable = [
        'name',
    ];

}
