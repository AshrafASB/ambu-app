<?php

namespace App\Providers;

use App\Policies\SenderPolicy;
use App\Sender;
use App\senderUser;
use App\User;
use Laravel\Passport\Passport;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        senderUser::class => SenderPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();

        Gate::before(function ($user) {
            if ($user->level=='Admin' ){
                return true;
            }
        });


        Gate::define('create.sender',function (User $user){
           return $user->hasPermission('create.sender');
        });

        Gate::define('create.reception',function (User $user){
            return $user->hasPermission('create.reception');

        });

        Gate::define('create.ambulances',function (User $user){
            return $user->hasPermission('create.ambulances');
        });

        Gate::define('view.sender',function (User $user){
            return $user->hasPermission('view.sender');
        });

        Gate::define('view.reception',function (User $user){
            return $user->hasPermission('view.reception');
        });
    }
}