<?php

namespace App\Http\Controllers\API;

use App\bag;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BagController extends Controller
{
    public function index(){
        $bag = bag::all();
        $name = "Bag";
        return parent::success($name,$bag);
    }

    public function show($id){
        try{
            $bag = bag::findOrFail($id);
            $name = "Bag";
            return parent::success($name , $bag);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("The content Bag not found");
        }
    }


}
