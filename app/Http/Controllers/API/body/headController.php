<?php

namespace App\Http\Controllers\API\body;

use App\head_infection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class headController extends Controller
{
    
    public function index(){

            $head = head_infection::all();
            $name = "Head";
            return parent::success($name,$head);
    }


    public function show($id){
        try{
            $head = head_infection::findOrFail($id);

            return parent::success($head);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("infection head is not found");
        }
    }
}
