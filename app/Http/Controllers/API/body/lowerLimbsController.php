<?php

namespace App\Http\Controllers\API\body;

use App\lower_limbs_infection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class lowerLimbsController extends Controller
{

    public function index(){

        $lowerLimbs = lower_limbs_infection::all();
        $name = "Lower_Limbs";

        return parent::success($name ,$lowerLimbs);
    }


    public function show($id){
        try{
            $lowerLimbs = lower_limbs_infection::findOrFail($id);
            return parent::success($lowerLimbs);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("infection lower Limbs is not found");
        }
    }
}
