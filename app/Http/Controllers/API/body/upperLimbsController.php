<?php

namespace App\Http\Controllers\API\body;

use App\upper_limbs_infection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class upperLimbsController extends Controller
{
    public function index(){

        $upperLimbs = upper_limbs_infection::all();
        $name = "Upper_Limbs";
        return parent::success($name,$upperLimbs);
    }


    public function show($id){
        try{
            $upperLimbs = upper_limbs_infection::findOrFail($id);
            return parent::success($upperLimbs);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("infection upper Limbs is not found");
        }
    }
}
