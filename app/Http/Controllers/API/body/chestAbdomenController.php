<?php

namespace App\Http\Controllers\API\body;

use App\chest_abdomen_infection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class chestAbdomenController extends Controller
{
    public function index(){

        $chestAbdomen = chest_abdomen_infection::all();
        $name = "Chest_Abdomen";
        return parent::success($name , $chestAbdomen);
    }


    public function show($id){
        try{
            $chestAbdomen = chest_abdomen_infection::findOrFail($id);
            return parent::success($chestAbdomen);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("infection chest Abdomen is not found");
        }
    }
}
