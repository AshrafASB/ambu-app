<?php

namespace App\Http\Controllers\API;

use App\body;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BodyController extends Controller
{

    public function index(){
        $body = body::all();
        $name = "body";
        return parent::success($name,$body);
    }

    public function show($id){
        try{
            $body = body::findOrFail($id);
            $name = "body";
            return parent::success($name , $body);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("The content body not found");
        }
    }

}
