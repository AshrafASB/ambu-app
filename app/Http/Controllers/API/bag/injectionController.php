<?php

namespace App\Http\Controllers\API\bag;

use App\injection_category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class injectionController extends Controller
{
    public function index(){
        $injection = injection_category::all();
        $name = "injection";
        return parent::success($name , $injection);
    }

    public function show($id){
        try{
            $injection = injection_category::findOrFail($id);
            return parent::success($injection);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("injection is not found");
        }
    }
}
