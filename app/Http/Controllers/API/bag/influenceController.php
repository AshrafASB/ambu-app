<?php

namespace App\Http\Controllers\API\bag;

use App\influence_category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class influenceController extends Controller
{
    public function index(){
        $influence = influence_category::all();
        $name = "influence";
        return parent::success($name , $influence);
    }

    public function show($id){
        try{
        $influence = influence_category::findOrFail($id);
        return parent::success($influence);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("influence is not found");
        }
    }
}

