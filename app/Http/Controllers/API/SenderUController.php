<?php

namespace App\Http\Controllers\API;

use App\senderUser;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\validator;
use Prophecy\Exception\Doubler\MethodNotFoundException;

class SenderUController extends Controller
{

    public function index (){

        $senderU = senderUser::all();
        $name  = 'sender';
        return parent::success($name,$senderU) ;
    }


    public function show($id){
        try{
            $senderU = senderUser::findOrFail($id);
            $name  = 'sender';
            return parent::success($name,$senderU);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("The content Bag not found");
        }
    }




    public function store(Request $request){

        $validation = Validator::make($request->all(),$this->rules());
        if ($validation->fails()){
            return parent::error($validation->errors());
        }
        $request['password'] = Hash::make($request->input('password')) ;
//        dd($request->all());
        $name  = 'sender';
        return parent::success($name,'User and Password is True');
    }


    public function  update(Request $request , $id){
        $validation =  validator::make($request->all(), $this->rules());
        if ($validation->fails())
            return parent::error($validation->errors());

        try{
            $senderU =  senderUser::findOrFial($id);
            $senderU->fill($request->all());
            $senderU->update();
            $name = "Ambulance";

            return parent::success($name,$senderU);
        }catch(ModelNotFoundException $modelNotFoundException){
            return parent::error('user is not found',404);

        }
    }

    public function destroy($id){
        try{
            $senderU = senderUser::findOrFail($id);
            $result = $senderU->delete();
            if($result === TRUE){
                $name = "Ambulance";
                return parent::success($name,$senderU);}
            return self::error('Something went error');

        }catch (MethodNotFoundException $methodNotFoundException){
            return parent::error('User is not found');
        }
    }
    private function rules($id=null){
        $rules = [
            'userName'=>'required|min:3|unique:SenderU,userName'.($id != null ?','.$id :''),
            'password'=>'required|min:6 '

        ];
        // to unchange the password
        if($id)
            unset($rules['password']);
            return $rules;

    }

}



















//        dd($request->all());




// to save data in dataBase
//        $senderU = new senderUser();
//        $senderU->fill($request->all());
//        $senderU->save();


// rules
//            'F_name'=>'required|min:3',
//            'email'=>'required|email|unique:SenderU,email',

// في حال بدك ترجع البيانات الي بدك اياها من الجوال تاني اله
//        $senderU = senderUser::created($request->all());
//        return parent::success($senderU);
