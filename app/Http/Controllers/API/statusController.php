<?php
namespace App\Http\Controllers\API;

use App\senderUser;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\statusNotification;
use App\statuses;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\validator;
use Prophecy\Exception\Doubler\MethodNotFoundException;


class statusController extends Controller
{
    public function index(){

    }

    public function store(){

        // dd(statuses::findorfail(18));
        $request = request();

        $senderid = $request->sender_id;

        $patient_id = $request->patient_id;
        
        $ambulance_id = $request->ambulance_id;
       $status =  statuses::create([
            'name' => $patient_id,
            'Gender' => null,
            'arrived' => null,
            'sender_id' => $senderid,
            'ambulance_id' => $ambulance_id,
            'Body' => serialize($request->injuries),
            'Bag' => serialize($request->medicines),
            
        ]);



        try{
            $senderU = senderUser::findOrFail($senderid);


            $users =  User::all();

            foreach($users as $user){
                $user->notify(new statusNotification($senderU));

            }
            $name = "notify";
            return parent::success($name  , $senderU);
        }catch (ModelNotFoundException $modelNotFoundException){
            return parent::error("The content Bag not found");
        }
    }
}
