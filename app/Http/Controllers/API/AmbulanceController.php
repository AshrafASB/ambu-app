<?php

namespace App\Http\Controllers\API;

use App\ambulance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Prophecy\Exception\Doubler\MethodNotFoundException;

class AmbulanceController extends Controller
{
    public function index(){
        $ambulance = ambulance::all();
        $name = "Ambulance";

        return parent::success($name , $ambulance);
    }

    public function show($id){
        try{
            $ambulance = ambulance::findOrFail($id);
            $name = "Ambulance";

            return parent::success($name ,$ambulance);
        }catch (MethodNotFoundException $methodNotFoundException){
            return parent::error("Ambulance is not Found") ;
        }



    }

    public function showName($name){
        try{
            $ambulance = ambulance::findOrFail($name);
            return parent::success($ambulance);
        }catch (MethodNotFoundException $methodNotFoundException){
            return parent::error("Ambulance is not Found") ;
        }



    }


}
