<?php

namespace App\Http\Controllers\Auth;

use App\senderUser;
//use phpseclib\Crypt\Hash;
use Validator;
use Illuminate\Support\Facades\Hash;

use App\Sender;
use App\User;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;

class ApiTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public $successStatus = 200;


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $username = request('username');
        $password = request('password');


        $Sender=senderUser::where('userName','=',$username)->first();
        $id = $Sender->id;


        $password =  Hash::check($password, $Sender->password);
        if($password){
            $success['token']=$Sender->createToken('MyApp')->accessToken;
            return response()->json(["id"=>$id,'success' => $success], $this->successStatus);
        }
      else{
          return response()->json(['error'=>'Unauthorised'], 401);
      }

   }
//            $Sender= senderUser::create(['api_token' => $request[$success['token']], ]);


//
//    public function register(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'F_name' => 'required',
//            'L_name' => 'ashraf11',
//            'username' => 'asedd',
//            'email' => 'required|email',
//            'Birthday' => null,
//            'password' => 'required',
//            'c_password' => 'required|same:password',
//            'ambulance_id' => 1
//        ]);
//        if ($validator->fails()) {
//            return response()->json(['error'=>$validator->errors()], 401);
//        }
//        $input = $request->all();
//        $input['password'] = bcrypt($input['password']);
//        $user = senderUser::create($input);
//        $success['token'] =  $user->createToken('MyApp')-> accessToken;
//        $success['name'] =  $user->name;
//        return response()->json(['success'=>$success], $this-> successStatus);
//    }
//

    public function index()
    {
        //
        return view('auth.api-token',[
            'token' =>Auth::senderUser()->api_token,
        ]);
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $Sender = Auth::senderUser();
        $Sender->api_token = Str::random(80);
        $Sender->save();

        return redirect()->action('index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Sender = Auth::senderUser();
        $Sender->api_token = Str::random(80);
        $Sender->save();

        return redirect()->action('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Sender = Auth::senderUser();
        $Sender->api_token = null;
        $Sender->save();

        return redirect()->action('index');
    }
}
