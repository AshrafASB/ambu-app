<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/login';

    public function redirectTo()
    {
        

        if (Auth::check() && Auth::user()->level == 'Admin') {
            // dd(Auth::user()->level);
            return '/admin/senders';
        } else if (Auth::check() && Auth::user()->level == 'reception') {
            return '/reception/statuses/';
        }
        // dd(Auth::user()->level);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



     public function username()
    {
        return 'userName';
    }

}
