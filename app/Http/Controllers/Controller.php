<?php

namespace App\Http\Controllers;

use App\status;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function success($name, $message, $status = 200)
    {

        return response()->json(
            ['status' => 'success',
                'name' => $name,
                'data' => $message,
                'errors' => 0
            ], $status)->header('Content-Type', 'application/json');
    }

    public static function error($message, $status = 400)
    {

        $messageCount = 1;
//        dd($message);
        if (is_array($message)) {
            $messageCount = sizeof($message);
        } elseif ($message instanceof Collection) {
            $messageCount = $message->count();
        }

        if ($message instanceof MessageBag)
            $message = $message->first();
        return response()->json(
            ['status' => 'error',
                'data' => $message,
                'errors' => $messageCount
            ],
            $status)->header('Content-Type', 'application/json');

    }
}
