<?php

namespace App\Http\Controllers\Admin;

use App\bag;
use App\body;
use App\Custom_Triates\R_M_statuses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\status;
use App\statuses;
use App\statusPatient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use PDF;
use Redirect;


class statusesController extends Controller
{

    use R_M_statuses;



    public function ReadNote($id)
    {
        Auth::user()->unreadnotifications()->findorfail($id)->markAsRead();
        $statuses = statuses::all();

        return redirect()->route('admin.statuses.index');
    }
    public function index()
    {

        $seraiNumber = request()->seraiNumber;

        $items = statuses::when($seraiNumber, function ($query, $seraiNumber) {
            $query->where('name', 'LIKE', '%' . $seraiNumber . '%');
        })->latest()->get();
        // dd($items);
        return view('admin.statues.index', [
            'stats' => $items,
            'seraiNumber' => $seraiNumber,
        ]);
    }


    public function view_injuers($id)
    {
        $item = statuses::findorfail($id);
        $body = unserialize($item->Body);
        if ($body) {
            $body = $body;
        } else {
            $body = [];
        }
        return view('admin.statues.view_injuers', [
            'items' => $body,
            'items2' => (count($body) > 2 ? array_slice($body, 2) : null),

        ]);
    }


    public function view_Bags($id)
    {

        $item = statuses::findorfail($id);
        $bag = unserialize($item->Bag);
        if ($bag) {
            $bag = $bag;
        } else {
            $bag = [];
        }
        return view('admin.statues.view_Bags', [
            'items' => $bag,
            // 'items2' => (count($bag) >2? array_slice($bag,2):null),

        ]);
    }


    public function getSonBody($name, $statuse)
    {
        $sonBody = '';

        switch ($name) {

            case 'Head':
                $sonBody = DB::table('head_infections')->where('id', '=', $statuse->SonBody_id)->first()->name;
                break;

            case 'upper_limbs':
                $sonBody = DB::table('upper_limbs_infections')->where('id', '=', $statuse->SonBody_id)->first()->name;
                break;


            case 'lower_limbs':
                $sonBody = DB::table('lower_limbs_infections')->where('id', '=', $statuse->SonBody_id)->first()->name;
                break;

            case 'chest_abdomen':
                $sonBody = DB::table('chest_abdomen_infections')->where('id', '=', $statuse->SonBody_id)->first()->name;
                break;
        }
        return $sonBody;
    }

    private function getSonBag($name, $statuse)
    {
        $sonBag = '';
        switch ($name) {
            case 'influence':
                $sonBag = DB::table('influence_categories')->where('id', '=', $statuse->SonBag_id)->first()->name;
                break;

            case 'injection':
                $sonBag = DB::table('injection_categories')->where('id', '=', $statuse->SonBag_id)->first()->name;
                break;
        }
        return $sonBag;
    }
    public function printPDF($id)
    {

        $item = statuses::findorfail($id);
        $body = unserialize($item->Body);
        if ($body) {
            $body = $body;
        } else {
            $body = [];
        }

        $bag = unserialize($item->Bag);
        // dd($bag);

        if ($bag) {
            $bag = $bag;
        } else {
            $bag = [];
        }

        $statuse =  statuses::findorfail($id);
        return view('pdf_view',[
            'content' => $statuse,
            'Head' => $body['Head'],
            'upper_limbs' => $body['upper_limbs'],
            'lower_limbs' => $body['lower_limbs5'],
            'chest' => $body['chest'],
            'influence' => $bag['influence'],
            'injection' => $bag['injection'],
        ]);

    }
}
