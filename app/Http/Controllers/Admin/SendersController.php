<?php


namespace App\Http\Controllers\Admin;

use App\ambulance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom_Triates\R_M_Senders;
use App\senderUser;
use App\User;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Prophecy\Exception\Doubler\MethodNotFoundException;

class SendersController extends Controller
{

    use R_M_Senders;

    public function index()
    {


        if (!Gate::allows('view.sender')){
            return view('errors.error403');
        }
        
       $request = request();
       $F_name = $request->F_name;
        $senders = senderUser::when($F_name, function ($query, $F_name) {
            $query->where('F_name', 'LIKE', '%' . $F_name . '%');
        })->latest()->get();
        
        return view('admin.Senders.index',[
            'senders' => $senders,
            'F_name' => $F_name,
        ]);
    }

    public function create()
    {

        if (!Gate::allows('create.sender')){
//            abort(403);
            return view('errors.error403');
        }
        $ambulances = ambulance::orderby('name')->get();
        return view('admin.Senders.create',[
            'ambulances' => $ambulances,
        ]);
    }
    
    public function store(Request $request)
    {
// dd($request['Ambulance']);
        $request->validate($this->rules(), $this->messages());
        $date = $request['Birthdate'];
        $newDate = date("Y-m-d", strtotime($date));

        $user =  senderUser::create([
            'F_name' => $request['firstName'],
            'L_name' => $request['lastName'],
            'username' => $request['username'],
            'ambulance_id' => $request['Ambulance'],
            'Birthday' => $newDate,
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->route('admin.senders.index');
    }
    
    
    
    
    public function show()
    {
        $request = request();
        // dd($request->resp_id);
        $user = senderUser::where('id','=',$request->sender_id)->first();
    
        return response()->json([
            'success' => 2,
            'sender_data'=>$user,
            'ambulance' => $user->Ambulance->name
           
        ]);

        // return $user;
    }
    
    

    public function edit($id)
    {

        $ambulances = Ambulance::orderby('name')->get();
      
        
        
       $user = senderUser::findorFail($id);


       $date = $user['Birthday'];
       $newDate = date("m/d/Y", strtotime($date));


        return view('admin.Senders.edit',[
            'user' => $user,
            'newDate' => $newDate,
            'ambulances' => $ambulances,
        ]);
        
    }


    public function update(Request $request, $id)
    {
        $request->validate($this->rules($id), $this->messages());
        $date = $request['Birthdate'];
        $newDate = date("Y-m-d", strtotime($date));

      

        $user = senderUser::findorFail($id);

        
         $user->update([
            'F_name' => $request['firstName'],
            'L_name' => $request['lastName'],
            'username' => $request['username'],
            'ambulance_id' => $request['Ambulance'],
            'password' => Hash::make($request['password']),
            'Birthday' => $newDate,
        ]);
        return redirect()->route('admin.senders.index');

//        dd($request->firstName);
    }

   
    
    public function destroy()
    {

        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $send_id = $request->sender_id;
            senderUser::findorFail($send_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this sender',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }


}
