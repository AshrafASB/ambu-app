<?php

namespace App\Http\Controllers\Admin\body;

use App\body;
use App\chest_abdomen_infection;
use App\Custom_Triates\R_M_chest_abdomen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class chest_abdomen_infectionsController extends Controller
{
    use R_M_chest_abdomen;
    

    public function index()
    {



        $request = request();
        $name = $request->name;
        $chest_abdomen = chest_abdomen_infection::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        

        // $chest_abdomen = chest_abdomen_infection::all();
        return view('admin.body.chest_abdomen_infections.index', [
            'chest_abdomen_infections' => $chest_abdomen,
            'name' => $name
        ]);
    }


    
    public function create()
    {

        return view('admin.body.chest_abdomen_infections.create',[
            'body' => body::all()
        ]);
    }


    




    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());

        // $chest_abdomen =  body::where('name', '=', 'chest_abdomen')->first();
        // $chest_abdomen_id = 0;
        // if ($chest_abdomen) {
        //     $chest_abdomen_id = $chest_abdomen->id;
        // }
        $chest_abdomen_infections =  chest_abdomen_infection::create([
            'name' => $request['name'],
            'chest_abdomen_id' => $request->chest_abdomen
        ]);
        return redirect()->route('admin.body.chest_abdomen_infections.index');
    }



    
    public function edit($id)
    {
        $chest_abdomen_infection = chest_abdomen_infection::findorFail($id);



        return view('admin.body.chest_abdomen_infections.edit', [
            'chest_abdomen_infection' => $chest_abdomen_infection,
            'body' => body::all(),

        ]);
    }



    public function update(Request $request, $id)
    {

        $chest_abdomen_infections = chest_abdomen_infection::findorFail($id);
        $request->validate($this->rules(), $this->messages());
        $chest_abdomen_infections->update([
            'name' => $request['name'],
            'chest_abdomen_id' => $request->chest_abdomen

        ]);
        return redirect()->route('admin.body.chest_abdomen_infections.index');
    }



    

    
    public function destroy()
    {
        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $chest_abdomen_infection_id = $request->chest_abdomen_infection_id;
            chest_abdomen_infection::findorFail($chest_abdomen_infection_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }










}
