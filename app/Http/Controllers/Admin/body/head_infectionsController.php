<?php

namespace App\Http\Controllers\Admin\body;

use App\body;
use App\Custom_Triates\R_M_head_infections;
use App\head_infection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class head_infectionsController extends Controller
{



    use R_M_head_infections;

    public function index()
    {


        $request = request();
        $name = $request->name;
        $head_infections = head_infection::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        


        // $head_infections = head_infection::all();
        return view('admin.body.head_infections.index', [
            'head_infections' => $head_infections,
            'name' => $name
        ]);
    }

    public function create()
    {

        return view('admin.body.head_infections.create',[
            'body' => body::all(),
        ]);
    }





    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());
        $head_infections =  head_infection::create([
            'name' => $request['name'],
            'head_id' => $request->head_id,
        ]);
        return redirect()->route('admin.body.head_infections.index');
    }



    // public function show()
    // {
    //     $request = request();
    //     $ambulance = Ambulance::where('id', '=', $request->ambulance_id)->first();
    //     return response()->json([
    //         'success' => 2,
    //         'ambulance_data' => $ambulance,
    //     ]);

    //     // return $user;
    // }




    public function edit($id)
    {
        $head_infections = head_infection::findorFail($id);



        return view('admin.body.head_infections.edit', [
            'body' => body::all(),
            'head_infections' => $head_infections,
        ]);
    }



    public function update(Request $request, $id)
    {
        $head_infections = head_infection::findorFail($id);
        // return $head_infections->name;
        $request->validate($this->rules(), $this->messages());
        $head_infections->update([
            'name' => $request['name'],
            'head_id' => $request->head_id,

        ]);
        return redirect()->route('admin.body.head_infections.index');
    }


    

      
    public function destroy()
    {
        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $head_infection_id = $request->head_infection_id;
            head_infection::findorFail($head_infection_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }

}
