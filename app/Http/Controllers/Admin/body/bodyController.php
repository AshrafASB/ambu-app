<?php

namespace App\Http\Controllers\Admin\body;

use App\Ambulance;
use App\body;
use App\Custom_Triates\R_M_Body;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class bodyController extends Controller
{
    use R_M_Body;

    public function index(){


        $request = request();
        $name = $request->name;
        $parts = body::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        

         
        // $parts = body::all();
        return view('admin.body.body.index',[
            'parts' => $parts,
            'name' => $name
        ]);
    }

    public function create(){

        return view('admin.body.body.create');
    }





    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());
        $body =  body::create([
            'name' => $request['name'],
        ]);
        return redirect()->route('admin.body.body.index');
    }



    public function show()
    {
        $request = request();
        $ambulance = Ambulance::where('id','=',$request->ambulance_id)->first();
        return response()->json([
            'success' => 2,
            'ambulance_data'=>$ambulance,
        ]);

        // return $user;
    }




    public function edit($id)
    {
        $part= body::findorFail($id);



        return view('admin.body.body.edit',[
            'part' => $part,
        ]);

    }



    public function update(Request $request, $id)
    {
        $part = body::findorFail($id);
        $request->validate($this->rules(), $this->messages());
        $part->update([
            'name' => $request['name'],
        ]);
        return redirect()->route('admin.body.body.index');
    }



    

    
    public function destroy()
    {
        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $body_id = $request->body_id;
            body::findorFail($body_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }


}
