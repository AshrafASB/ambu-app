<?php

namespace App\Http\Controllers\Admin\body;

use App\body;
use App\Custom_Triates\R_M_upper_limbs_infections;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\upper_limbs_infection;

class upper_limbs_infectionsController extends Controller
{
    use R_M_upper_limbs_infections;



    public function index()
    {


        $request = request();
        $name = $request->name;
        $upper_limbs_infections = upper_limbs_infection::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        

        // $upper_limbs_infections = upper_limbs_infection::all();
        return view('admin.body.upper_limbs_infections.index', [
            'upper_limbs_infections' => $upper_limbs_infections,
            'name' => $name,
        ]);
    }



    public function create()
    {

        return view('admin.body.upper_limbs_infections.create',[
            'body' => body::all(),
        ]);
    }




    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());

        // $upper_limbs_infections =  body::where('name', '=', 'upper_limbs')->first();
        // // dd($upper_limbs_infections->name);
        // $upper_limbs_infections_id = 0;
        // if ($upper_limbs_infections) {
        //     $upper_limbs_infections_id = $upper_limbs_infections->id;
        // }
        $upper_limbs_infections =  upper_limbs_infection::create([
            'name' => $request['name'],
            'upper_limbs_id' => $request->upper_limbs_infections_id
        ]);
        return redirect()->route('admin.body.upper_limbs_infections.index');
    }




    public function edit($id)
    {
        $upper_limbs_infections = upper_limbs_infection::findorFail($id);



        return view('admin.body.upper_limbs_infections.edit', [
            'body' => body::all(),
            'upper_limbs_infections' => $upper_limbs_infections,
        ]);
    }



    public function update(Request $request, $id)
    {
        $upper_limbs_infections = upper_limbs_infection::findorFail($id);
        $request->validate($this->rules(), $this->messages());
        $upper_limbs_infections->update([
            'name' => $request['name'],
            'upper_limbs_id' => $request->upper_limbs_infections_id

        ]);
        return redirect()->route('admin.body.upper_limbs_infections.index');
    }





  
    
      
    public function destroy()
    {
        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $upper_limbs_infection_id = $request->upper_limbs_infection_id;
            upper_limbs_infection::findorFail($upper_limbs_infection_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }

}
