<?php

namespace App\Http\Controllers\Admin\body;

use App\body;
use App\Custom_Triates\R_M_lower_limbs_infections;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\lower_limbs_infection;

class lower_limbs_infectionsController extends Controller
{

    use R_M_lower_limbs_infections;


    
    public function index()
    {


        $request = request();
        $name = $request->name;
        $lower_limbs_infections = lower_limbs_infection::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        

        // $lower_limbs_infections = lower_limbs_infection::all();
        return view('admin.body.lower_limbs_infections.index', [
            'lower_limbs_infections' => $lower_limbs_infections,
            'name' => $name
        ]);
    }

    public function create()
    {

        return view('admin.body.lower_limbs_infections.create',[
            'body' => body::all(),
        ]);
    }



    
    
    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());
        $lower_limbs_infections =  lower_limbs_infection::create([
            'name' => $request['name'],
            'lower_limbs_id' => $request->lower_limbs_infections_id
        ]);
        return redirect()->route('admin.body.lower_limbs_infections.index');
    }


    public function edit($id)
    {
        $lower_limbs_infections = lower_limbs_infection::findorFail($id);



        return view('admin.body.lower_limbs_infections.edit', [
            'lower_limbs_infections' => $lower_limbs_infections,
            'body' => body::all(),

        ]);
    }

    public function update(Request $request, $id)
    {
        $lower_limbs_infections = lower_limbs_infection::findorFail($id);
        $request->validate($this->rules(), $this->messages());
        $lower_limbs_infections->update([
            'name' => $request['name'],
            'lower_limbs_id' => $request->lower_limbs_infections_id

        ]);
        return redirect()->route('admin.body.lower_limbs_infections.index');
    }



       
    public function destroy()
    {
        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $lower_limbs_infection_id = $request->lower_limbs_infection_id;
            lower_limbs_infection::findorFail($lower_limbs_infection_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }












}
