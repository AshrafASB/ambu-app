<?php

namespace App\Http\Controllers\admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom_Triates\R_M_Receptions;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prophecy\Exception\Doubler\MethodNotFoundException;


use Illuminate\Support\Facades\Gate;

//
//use PDF;
//use Redirect;

class ReceptionsController extends Controller
{

    use R_M_Receptions;

    public function index()
    {

        


        if (!Gate::allows('view.reception')){
            return view('errors.error403');
        }
        


        $request = request();
        $F_name = $request->F_name;
         $resps = User::when($F_name, function ($query, $F_name) {
             $query->where('F_name', 'LIKE', '%' . $F_name . '%');
         })->latest()->get();
        
         
        return view('admin.Receptions.index',[
            'resps' => $resps,
            'F_name' => $F_name,
        ]);
    }

    public function create()
    {

        if (!Gate::allows('create.reception')){
            return view('errors.error403');
        }

        return view('admin.Receptions.create');
    }

    public function store(Request $request)
    {

        $request->validate($this->rules(), $this->messages());
        $date = $request['Birthdate'];
        $newDate = date("Y-m-d", strtotime($date));

        $user =  User::create([
            'F_name' => $request['firstName'],
            'L_name' => $request['lastName'],
            'username' => $request['username'],
            'Birthday' => $newDate,
            'level' => 'reception',
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->route('admin.Receptions.index');
    }

    public function show()
    {
        $request = request();
        // dd($request->resp_id);
        $user = User::where('id','=',$request->resp_id)->first();
        return response()->json([
            'success' => 2,
            'resp_data'=>$user,
            'message' => 'Dislike!'
        ]);

        // return $user;
    }


    public function edit($id)
    {
       $user = User::findorFail($id);


       $date = $user['Birthday'];
       $newDate = date("m/d/Y", strtotime($date));


        return view('admin.Receptions.edit',[
            'user' => $user,
            'newDate' => $newDate,
        ]);
        
    }


    public function update(Request $request, $id)
    {

        $request->validate($this->rules($id), $this->messages());
        $date = $request['Birthdate'];
        $newDate = date("Y-m-d", strtotime($date));

      

        $user = User::findorFail($id);

        
         $user->update([
            'F_name' => $request['firstName'],
            'L_name' => $request['lastName'],
            'username' => $request['username'],
            'Birthday' => $newDate,
            'password' => Hash::make($request['password']),
            'email' => $request['email'],
        ]);
        return redirect()->route('admin.Receptions.index');

        dd($request->firstName);
    }


    public function destroy()
    {

        $request = request();
        $resp_id = $request->resp_id;
        User::findorFail($resp_id)->delete();
        return response()->json([
            'success' => 2,
          
        ]);


    }


//    public function printPDF()
//    {
////        dd(55);
//
//        // This  $data array will be passed to our PDF blade
//        $users =  User::all();
//
//        $data = [
//            'title' => 'Patients Ambulance Report',
//            'heading' => 'Report the patients condition of ambulance',
//            'content' => $users->first(),
//        ];
////        $carbon = new Carbon();
//
//        $pdf = PDF::loadView('pdf_view', $data);
//        return $pdf->download(now());
//    }
}
