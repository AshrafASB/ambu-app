<?php

namespace App\Http\Controllers\Admin;

use App\ambulance;
use App\body;
use App\Custom_Triates\R_M_Ambulances;
use App\Custom_Triates\R_M_Body;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AmbulancesController extends Controller
{

    use R_M_Ambulances;

    public function index()
    {

        $request = request();
        $name = $request->name;
        $ambulances = ambulance::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        

        // $ambulances = ambulance::orderby('name')->get();
        return view('admin.Ambulances.index',[
            'ambulances' => $ambulances,
            'name' => $name,
        ]);
    }

    public function create()
    {

//         $count =  DB::table('users_roles')
//         ->join('roles_permissions','users_roles.roles_id','=','roles_permissions.role_id')
//         ->join('permissions','permissions.id','=','roles_permissions.permission_id')
//         ->where('users_roles.users_id','=',dd(Auth::id()))
//         ->where('permissions.code','=','create.ambulances')->count();
// dd($count);
// dd(Gate::allows('create.ambulances'));

        if (!Gate::allows('create.ambulances')){

            return view('errors.error403');
        }
        return view('admin.Ambulances.create');
    }

    public function store(Request $request)
    {

        $request->validate($this->rules(), $this->messages());
        $ambulance =  ambulance::create([
            'name' => $request['name'],
        ]);
        return redirect()->route('admin.ambulances.index');
    }


    public function show()
    {
        $request = request();
        $ambulance = ambulance::where('id','=',$request->ambulance_id)->first();
        return response()->json([
            'success' => 2,
            'ambulance_data'=>$ambulance,
        ]);

        // return $user;
    }




    public function edit($id)
    {
       $ambulance = ambulance::findorFail($id);



        return view('admin.Ambulances.edit',[
            'ambulance' => $ambulance,
        ]);

    }



    public function update(Request $request, $id)
    {
        $ambulance = ambulance::findorFail($id);
        $request->validate($this->rules(), $this->messages());
        $ambulance->update([
            'name' => $request['name'],
        ]);
        return redirect()->route('admin.ambulances.index');
    }



    

    public function destroy()
    {

        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $ambulance_id = $request->ambulance_id;
            ambulance::findorFail($ambulance_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }


}
