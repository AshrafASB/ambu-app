<?php

namespace App\Http\Controllers\Admin\bags;

use App\bag;
use App\Custom_Triates\bags\R_M_injection_categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\injection_category;

class injection_categoriesController extends Controller
{
  use R_M_injection_categories;

   
  public function index(){



    $request = request();
    $name = $request->name;
    $injection_categories = injection_category::when($name, function ($query, $name) {
         $query->where('name', 'LIKE', '%' . $name . '%');
     })->latest()->get();
    


    //   $injection_categories = injection_category::all();
      return view('admin.bags.injection_categories.index',[
          'injection_categories' => $injection_categories,
          'name' => $name,
      ]);
  }


      
  public function create()
  {

      return view('admin.bags.injection_categories.create',[
          'bag' => bag::all(),
      ]);
  }


  
  public function store(Request $request)
  {
      $request->validate($this->rules(), $this->messages());

    //   $item =  bag::where('name', '=', 'injection')->first();
    //   $item_id = 0;
    //   if ($item) {
    //       $item_id = $item->id;
    //   }
      injection_category::create([
          'name' => $request['name'],
          'injection_id' => $request->injection_id
      ]);
      return redirect()->route('admin.bags.injection_categories.index');
  }


  
  public function edit($id)
  {
      $injection_category = injection_category::findorFail($id);



      return view('admin.bags.injection_categories.edit', [
          'injection_category' => $injection_category,
          'bag' => bag::all(),

      ]);
  }



  
  public function update(Request $request, $id)
  {

      $item = injection_category::findorFail($id);
      $request->validate($this->rules(), $this->messages());
      $item->update([
          'name' => $request['name'],
          'injection_id' => $request->injection_id

      ]);
      return redirect()->route('admin.bags.injection_categories.index');
      
  }



  



  public function destroy()
  {

      try{
          // هنا علشان ممنوع يحذف الابو وفيه له ابناء
          $request = request();
          $injection_category_id = $request->injection_category_id;
          injection_category::findorFail($injection_category_id)->delete();
      }catch(QueryException $e){
      return response()->json([
          'success' => 'can\'t delete this item',
      ]);    
      }
      return response()->json([
          'success' => 2,
        
      ]);

  }



  

}
