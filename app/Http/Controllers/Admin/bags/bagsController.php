<?php

namespace App\Http\Controllers\Admin\bags;

use App\Custom_Triates\bags\R_M_bags;
use App\bag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class bagsController extends Controller
{
    use R_M_bags;

   
    public function index(){

        $request = request();
        $name = $request->name;
        $bags = bag::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        

        // $bags = bag::all();
        return view('admin.bags.bags.index',[
            'bags' => $bags,
            'name' => $name
        ]);
    }

    public function create(){

        return view('admin.bags.bags.create');
    }


    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());
        $bags =  bag::create([
            'name' => $request['name'],
        ]);
        return redirect()->route('admin.bags.bags.index');
    }



    

    public function edit($id)
    {
        $bags= bag::findorFail($id);



        return view('Admin.bags.bags.edit',[
            'bags' => $bags,
        ]);

    }


    
    public function update(Request $request, $id)
    {
        $bags = bag::findorFail($id);
        $request->validate($this->rules(), $this->messages());
        $bags->update([
            'name' => $request['name'],
        ]);
        return redirect()->route('admin.bags.bags.index');
    }


    

    public function destroy()
    {

        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $bag_id = $request->bag_id;
            bag::findorFail($bag_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }










}
