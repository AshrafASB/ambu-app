<?php

namespace App\Http\Controllers\Admin\bags;

use App\bag;
use App\Custom_Triates\bags\R_M_influence_categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\influence_category;
use Illuminate\Database\QueryException;

class influence_categoriesController extends Controller
{
    
    use R_M_influence_categories;

   
    public function index(){


        $request = request();
        $name = $request->name;
        $influence_categories = influence_category::when($name, function ($query, $name) {
             $query->where('name', 'LIKE', '%' . $name . '%');
         })->latest()->get();
        

        // $influence_categories = influence_category::all();
        return view('admin.bags.influence_categories.index',[
            'influence_categories' => $influence_categories,
            'name' => $name
        ]);
    }

    
    public function create()
    {

        return view('admin.bags.influence_categories.create',[
            'bag' => bag::all(),
        ]);
    }

    
    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());

        // $influence_categorie =  bag::where('name', '=', 'influence')->first();
        // $influence_categorie_id = 0;
        // if ($influence_categorie) {
        //     $influence_categorie_id = $influence_categorie->id;
        // }
        $influence_categorie =  influence_category::create([
            'name' => $request['name'],
            'influence_id' => $request->influence_categorie_id
        ]);
        return redirect()->route('admin.bags.influence_categories.index');
    }


    
    
    public function edit($id)
    {
        $influence_category = influence_category::findorFail($id);



        return view('admin.bags.influence_categories.edit', [
            'influence_category' => $influence_category,
            'bag' => bag::all(),

        ]);
    }



    public function update(Request $request, $id)
    {

        $influence_category = influence_category::findorFail($id);
        $request->validate($this->rules(), $this->messages());
        $influence_category->update([
            'name' => $request['name'],
            'influence_id' => $request->influence_categorie_id

        ]);
        return redirect()->route('admin.bags.influence_categories.index');
        
    }



    
    
    public function destroy()
    {

        try{
            // هنا علشان ممنوع يحذف الابو وفيه له ابناء
            $request = request();
            $influence_category_id = $request->influence_category_id;
            influence_category::findorFail($influence_category_id)->delete();
        }catch(QueryException $e){
        return response()->json([
            'success' => 'can\'t delete this item',
        ]);    
        }
        return response()->json([
            'success' => 2,
          
        ]);

    }







}
