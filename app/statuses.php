<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class statuses extends Model
{


    protected $table = 'statuses';
    protected $guarded=[];

    public function Sender(){
        return $this->belongsTo(senderUser::class,'sender_id','id');
    }



    public function amblunce(){
        return $this->belongsTo(Ambulance::class,'ambulance_id','id');
    }



    public function parenBody(){
        return $this->belongsTo(body::class,'ParentBody_id','id');
    }


    public function parenBag(){
        return $this->belongsTo(bag::class,'ParentBag_id','id');
    }





}