<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lower_limbs_infections extends Model
{
    
    protected $table = 'lower_limbs_infections';
    protected $fillable = [
        'name','lower_limbs_id',
    ];
}
