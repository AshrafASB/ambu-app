<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class upper_limbs_infection extends Model
{
    protected $table = 'upper_limbs_infections';
    protected $fillable = [
        'name','upper_limbs_id',
    ];
}
