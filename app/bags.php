<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bags extends Model
{
    protected $table = 'bags';
    protected $fillable = [
        'name',
    ];

}
