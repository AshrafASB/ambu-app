<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class head_infection extends Model
{
    
    protected $table = 'head_infections';
    protected $fillable = [
        'name','head_id',
    ];

}
