<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SenderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }



    // public function before($user, $ability)
    // {
    //     // return false;
    //     if ($user->isSuperAdmin()) {
    //         return true;
    //     }
    // }
    
        
    
        public function index(User $user){
           return $user->hasPerm('senders.index');
        }

}
