<?php

namespace App;

//use App\Providers\AuthServiceProvider;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'F_name','L_name', 'email','username','Birthday' ,'level', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['birthday'];
    



    public function hasPerm($perName)
    {
        return userPerms::where('user_id','=',$this->id)->where('perName','=',$perName)->count();
    }

    public function isSuperAdmin(){
        return $this->super_Admin;
    }

    //كل يوزر له رولز
    public function roles(){
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    public function hasPermission($code){
       $count =  DB::table('users_roles')
                    ->join('roles_permissions','users_roles.roles_id','=','roles_permissions.role_id')
                    ->join('permissions','permissions.id','=','roles_permissions.permission_id')
                    ->where('users_roles.users_id','=',$this->id)
                    ->where('permissions.code','=',$code)
                    ->count();
        if ($count){
            return true;
        }else{
            return false;
        }

    }


}