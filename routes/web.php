<?php


Route::get('/FAQ', function(){return view('FAQ');})->name('FAQ');

Route::get('/logout',function(){
    Auth::logout();
    return redirect('/login');})->name('home.loguot');

Route::get('/login',function(){return "555";})->name('login');

Route::get('/done', function(){return 'done';})->name('status.done');

Route::get('/', function () {return view('welcome');})->name('home');

Auth::routes();


Route::get('/index', ['as' => 'index','uses' => 'metronicController@index']);


Route::namespace('Admin')->prefix('admin')->middleware('admin')->name('admin.')->group(
    function () {
        Route::namespace('body')->prefix('body')->name('body.')->group(
            function () {
                Route::get('/body/delete',['as' => 'body.delete','uses' => 'bodyController@destroy']);

                Route::get('/chest_abdomen_infection/delete',['as' => 'chest_abdomen_infection.delete','uses' => 'chest_abdomen_infectionsController@destroy']);

                Route::get('/head_infections/delete',['as' => 'head_infections.delete','uses' => 'head_infectionsController@destroy']);

                Route::get('/upper_limbs_infections/delete',['as' => 'upper_limbs_infections.delete','uses' => 'upper_limbs_infectionsController@destroy']);

                Route::get('/lower_limbs_infections/delete',['as' => 'lower_limbs_infections.delete','uses' => 'lower_limbs_infectionsController@destroy']);

                Route::resource('/body', 'bodyController');

                Route::resource('/head_infections', 'head_infectionsController');

                Route::resource('/chest_abdomen_infections', 'chest_abdomen_infectionsController');

                Route::resource('/upper_limbs_infections', 'upper_limbs_infectionsController');

                Route::resource('/lower_limbs_infections', 'lower_limbs_infectionsController');
            });


        Route::prefix('senders')->name('senders.')->group(
            function () {
                Route::get('/', ['as' => 'index', 'uses' => 'SendersController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'SendersController@create']);
                Route::get('/show', ['as' => 'show', 'uses' => 'SendersController@show']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'SendersController@edit']);

                Route::get('/delete', ['as' => 'delete', 'uses' => 'SendersController@destroy']);
                Route::post('/sotre', ['as' => 'store', 'uses' => 'SendersController@store']);
                Route::put('/update/{id}', ['as' => 'update', 'uses' => 'SendersController@update']);
            });


        Route::prefix('Receptions')->name('Receptions.')->group(
            function(){
                Route::get('/', ['as' => 'index', 'uses' => 'ReceptionsController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'ReceptionsController@create']);
                Route::get('/show', ['as' => 'show', 'uses' => 'ReceptionsController@show']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ReceptionsController@edit']);
                Route::get('/delete', ['as' => 'delete', 'uses' => 'ReceptionsController@destroy']);
                Route::post('/sotre', ['as' => 'store', 'uses' => 'ReceptionsController@store']);
                Route::put('/update/{id}', ['as' => 'update', 'uses' => 'ReceptionsController@update']);
            });


        Route::prefix('ambulances')->name('ambulances.')->group(
            function(){
                Route::get('/', ['as' => 'index', 'uses' => 'AmbulancesController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'AmbulancesController@create']);
                Route::get('/show', ['as' => 'show', 'uses' => 'AmbulancesController@show']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'AmbulancesController@edit']);
                Route::get('/delete', ['as' => 'delete', 'uses' => 'AmbulancesController@destroy']);
                Route::post('/sotre', ['as' => 'store', 'uses' => 'AmbulancesController@store']);
                Route::put('/update/{id}', ['as' => 'update', 'uses' => 'AmbulancesController@update']);
        });

                Route::get('convert/printpdf/{id}', ['as' => 'convert.printpdf', 'uses' => 'statusesController@printPDF']);
                Route::get('/statuses/ReadNote/{id}', ['as' => 'statuses.ReadNote', 'uses' => 'statusesController@ReadNote']);
                Route::get('/statuses/view-injuers/{id}', ['as' => 'statuses.view-injuers', 'uses' => 'statusesController@view_injuers']);
                Route::get('/statuses/view-bag/{id}', ['as' => 'statuses.view-Bags', 'uses' => 'statusesController@view_Bags']);
                // Route::get('/statuses/show',	['as' => 'statuses.showData', 'uses' => 'statusesController@show']);
                Route::resource('/statuses', 'statusesController');
                Route::resource('/profile', 'profileController');


        Route::namespace('bags')->prefix('bags/')->name('bags.')->group(
            function () {
                Route::get('/bags/delete',['as' => 'bags.delete','uses' => 'bagsController@destroy']);

                Route::get('/bags/influence_categories/delete',['as' => 'influence_categories.delete','uses' => 'influence_categoriesController@destroy']);

                Route::get('/bags/injection_categories/delete',['as' => 'injection_categories.delete','uses' => 'injection_categoriesController@destroy']);

                Route::resource('/bags', 'bagsController');
                Route::resource('/influence_categories', 'influence_categoriesController');
                Route::resource('/injection_categories', 'injection_categoriesController');
            });


    });


            Route::get('/admin/logout', function(){
                Auth::logout();
                return redirect('/login');})->name('admin.logout');


Route::namespace('Reception')->prefix('reception')->middleware('admin')->name('reception.')->group(
    function () {
        Route::prefix('test')->name('test.')->group(
            function () {
                Route::get('/', ['as' => 'index', 'uses' => 'TestController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'TestController@create']);
                Route::post('/sotre', ['as' => 'store', 'uses' => 'TestController@store']);
            });


        Route::name('statuses.')->group(
            function () {
            Route::get('/statuses/ReadNote/{id}', ['as' => 'ReadNote', 'uses' => 'statusesController@ReadNote']);
            Route::resource('/report', 'reportController');
            Route::get('/statuses/view-injuers/{id}', ['as' => 'view-injuers', 'uses' => 'statusesController@view_injuers']);
            Route::get('/statuses/view-bag/{id}', ['as' => 'view-Bags', 'uses' => 'statusesController@view_Bags']);
        });
        Route::resource('/statuses', 'statusesController');
        Route::resource('/profile', 'profileController');
    });

Route::get('DIS/PDF',function (){
    return view('DIS_PDF');
});
