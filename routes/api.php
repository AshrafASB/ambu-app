<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->post('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'Auth\ApiTokenController@login');
Route::post('register', 'Auth\ApiTokenController@register');



Route::group(['middleware'=>'auth:api'],function (){

    Route::post('senderU' , ['as'=>'senderU.store','uses'=>'API\SenderUController@store']);
//show all Sender
    Route::post('alluser', ['as'=>'user.index','uses'=>'API\SenderUController@index']);
    Route::post('user/{id}', ['as'=>'user.show','uses'=>'API\SenderUController@show']);
//Delete sender
    Route::delete('senderUDle/{id}',['as'=>'SenderU.destroy','uses'=>'API\SenderUController@destroy']);




//show all Ambulance
    Route::post('AllAmbulance', ['as'=>'ambulance.index','uses'=>'API\AmbulanceController@index']);
//show one Ambulance
    Route::post('Ambulance/{id}', ['as'=>'ambulance.show','uses'=>'API\AmbulanceController@show']);
    Route::post('AmbulanceName/{name}', ['as'=>'ambulance.showName','uses'=>'API\AmbulanceController@showName']);


//show Bag category
    Route::post('Allbag',['as'=>'bag.index','uses'=>'API\BagController@index']);
    Route::post('bag/{id}',['as'=>'bag.show','uses'=>'API\BagController@show']);
    //$influence
    Route::post('Allinfluence',['as'=>'influence.index','uses'=>'API\bag\influenceController@index']);
    Route::post('influence/{id}',['as'=>'influence.show','uses'=>'API\bag\influenceController@show']);
    //$injection
    Route::post('Allinjection',['as'=>'injection.index','uses'=>'API\bag\injectionController@index']);
    Route::post('injection/{id}',['as'=>'injection.show','uses'=>'API\bag\injectionController@show']);


//show Body category
    Route::post('AllBody',['as'=>'Body.index','uses'=>'API\BodyController@index']);
    Route::post('Body/{id}',['as'=>'Body.show','uses'=>'API\BodyController@show']);
    //head
    Route::post('Allhead',['as'=>'head.index','uses'=>'API\body\headController@index']);
    Route::post('head/{id}',['as'=>'head.show','uses'=>'API\body\headController@show']);
    // upperLimbs
    Route::post('AllupperLimbs',['as'=>'upperLimbs.index','uses'=>'API\body\upperLimbsController@index']);
    Route::post('upperLimbs/{id}',['as'=>'upperLimbs.show','uses'=>'API\body\upperLimbsController@show']);
    // lowerLimbs
    Route::post('AlllowerLimbs',['as'=>'lowerLimbs.index','uses'=>'API\body\lowerLimbsController@index']);
    Route::post('lowerLimbs/{id}',['as'=>'lowerLimbs.show','uses'=>'API\body\lowerLimbsController@show']);
    // upperLimbs
    Route::post('AllchestAbdomen',['as'=>'chestAbdomen.index','uses'=>'API\body\chestAbdomenController@index']);
    Route::post('chestAbdomen/{id}',['as'=>'chestAbdomen.show','uses'=>'API\body\chestAbdomenController@show']);
    Route::post('stutas',['as'=>'stutas.index','uses'=>'API\statusController@index']);
    Route::post('stutasStore',['as'=>'stutas.store','uses'=>'API\statusController@store']);

});
