@extends('base_layout.homeLayout.master_layout')
@section('title','statuses')

@section('style')
<style>
</style>
@endsection


    <!-- start navbar -->
    <header>
  
        <nav class="navbar navbar-expand-lg navbar-light ">
                <div class="container">
            
            <a class="navbar-brand  " href="{{route('home')}}" >
                <span>Ambu</span>
                <span>App</span>
            
             
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
           
            <div class="collapse navbar-collapse" id="main-nav">
              <ul class="navbar-nav ml-auto" style="margin-right: 190px !important;">
                <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}">الرئيسية  <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="#" > </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#" ></a>
                  </li>
                </ul>
              </div>
                <ul style="list-style: none;color:white;">
                <li class="nav-item dropdown dropalign">
                    <a class="nav-link dropdown-toggle welcome" style="color:white;margin-bottom: -15px;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      أهلا و سهلا
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#">لوحة المستقبل</a>
                      <a class="dropdown-item" href="#">تسجيل خروج</a>
                      <div class="dropdown-divider"></div>
                
                    </div>
                  </li>
              </ul>
             
            </div>
          </nav>
        </nav>
        </nav>
    </header>
    <!-- end navbar -->

@section('body')



    <main id="main">

        <!--==========================
      Frequently Asked Questions Section
    ============================-->
        <section id="faq">
            <div class="container">
                <ul id="faq-list" class="wow fadeInUp float-left col-lg-12" dir="rtl">
                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq1"> <i class="ion-android-remove"></i> ما هو تطبيق Ambu App ؟</a>
                        <div id="faq1" class="collapse" data-parent="#faq-list">
                            <p>
                                هو تطبيق طبي مصمم لأجهزة الأندرويد الذكية مرتبط مع موقع الكتروني ( داش بور ) يأخذ منه البيانات و المعلومات الازمة لعمله و تحسين مهمته هي ربط الإسعاف بالمستشفى الخاصة به , لكي ينقل حالة المرضى قبل وصول سيارة الإسعاف للمستشفى , حتى تقوم إدارة المستشفى بتجهيز
                                الأشياء الازمة للحالة القادمة قبل وصلها , تجنب إعطاء المريض أي أدوية أعطيت له و هو في الإسعاف حتى لا تحدث مضاعفات له .
                            </p>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#faq2" class="collapsed">لماذا نحتاج   التطبيق و ما هي أهميته ؟ <i class="ion-android-remove"></i></a>
                        <div id="faq2" class="collapse" data-parent="#faq-list">
                            <p>1- نحتاج هذا التطبيق لمعرفة حالات المرضى قبل وصولهم الى المستشفى , و استغلال الوقت الذي يمضيه المريض في الإسعاف لتجهيز الأمور الازمة له و معرفة ما تم إعطائه من إبر أو محاليل لتجنب إعطائه له مرة أخرى , و أيضا لزيادة التوثيق
                                الطبي و تجنب أي أخطاء قد يرتكبها المسعف أو الممرض أو الطبيب , لأنه يدرك تمام أن أي خطأ سيكون موثق عليه و سيحاسب عليه , فيعمل بكل تركيز و إنتباه , </p>
                            <p>2- لأن الدقائق المعدودة التي قد تستغرقها إدارة المستشفى في معرفة حالة المريض و تجهيز ما يلزمه قد تكون هذه الدقائق كفيلة بإنقاذ حياة ألاف من الحالات المستعجلة أو الحالات الحسساسة </p>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#faq3" class="collapsed">ما هي المميزات التي يقدمها التطبيق ؟ <i class="ion-android-remove"></i></a>
                        <div id="faq3" class="collapse" data-parent="#faq-list">
                            <p>1- السهولة في التعامل</p>
                            <p>2- المرونة في الاستخدام</p>
                            <p>3- الواجهات الجذابة و الواضحة للمسعفين في التعامل مع التطبيق</p>
                            <p>4- المجسمات الموجودة فيه التي تسهل اختيار منطقة إصابة المريض و إرسالها للإدارة </p>
                            <p>5- استخدام مبدأ في الوقت الحالى (real time) في إرسال الإشعارات لإدارة المستشفى</p>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#faq4" class="collapsed">من هي الجهة المستهدفة في المجتمع من هذا التطبيق و من هي الجهة المستفيدة منه ؟ <i class="ion-android-remove"></i></a>
                        <div id="faq4" class="collapse" data-parent="#faq-list">
                            <p>1- الجهة المستهدفة : جميع طبقات المجتمع ( المرضى ) أي الحالات التي تنقل في الإسعاف بمختلف أنواعها ( كبار السن - صغار السن - الشباب - الذكور - الإناث ) .</p>
                            <p>2- الجهة المستفيدة من التطبيق : المستشفيات الخاصة و العامة على حد سواء و المرضى الذين قد تنقذ حياتهم الدقائق المعدودة التي يساعد التطبيق في إختصارها .</p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq5" class="collapsed"> ما هي بينة و تركيبة  مشروع Ambu App ؟ <i class="ion-android-remove"></i></a>
                        <div id="faq5" class="collapse" data-parent="#faq-list">
                            <p> أولا /   بينة التطبيق ( المشروع ) كالتالي :
                                    مشروع Ambu App عبارة عن تطبيق أندرويد يستمد بيانات و يرسلها الى موقع ويب ( وحة التحكم) و موقع الويب عبارة عن ثلاث أقسام 
                                      1- لوحة التحكم الخاصة بالأدمن ( المسؤول الأعلى عن الموقع  ) : فيها كل الصلاحيات من إضافة , تعديل , حذف لجميع بيانات المستخدمين و بيانات التطبيق .
                                      2- لوحة التحكم الخاصة بمستقبل البيانات في المستشفى : مهمته إستقبال البيانات و الإشعارات من التطبيق التي تحمل بيانات المصابين و طباعة تقرير عن الحالة.
                                      3- صفحة ترويجية عن التطبيق مدعومة بالمميزات و الفيديو توضيحي بمكونات التطبيق .</p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq6" class="collapsed">من الذين يستخدمون ؟ <i class="ion-android-remove"></i></a>
                        <div id="faq6" class="collapse" data-parent="#faq-list">
                            <p> المسعفين في المستشفيات العامة و الخاصة </p>
                        </div>
                    </li>

                </ul>

            </div>
        </section>
        <!-- #faq -->

    </main>
    @endsection

    @section('script')
    
    
    <script>
    </script>
    
    
    @endsection