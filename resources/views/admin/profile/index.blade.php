@extends('base_layout.master_layout')
@section('title','statuses')

@section('style')
<style>
</style>
@endsection

@section('body')


			
		<!-- END: Left Aside -->
		<div class="m-grid__item m-grid__item--fluid m-wrapper">


			
	<div class="m-content">
			<div class="row">
				<div class="col-xl-3 col-lg-4">
					<div class="m-portlet m-portlet--full-height  ">
						<div class="m-portlet__body">
							<div class="m-card-profile">
								<div class="m-card-profile__title m--hide">
									Your Profile
								</div>
								<div class="m-card-profile__pic">
									<div class="m-card-profile__pic-wrapper">
										<img src="{{asset('default/assets/app/media/img/users/logo.png')}}" alt="">
									</div>
								</div>
								<div class="m-card-profile__details">
									<span class="m-card-profile__name">{{Auth::user()->F_name}} {{Auth::user()->L_name}}</span>
								<a href="#" class="m-card-profile__email m-link">{{Auth::user()->email}}</a>
								</div>
							</div>
							
						
						</div>
					</div>
				</div>
				<div class="col-xl-9 col-lg-8">
					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
						<div class="tab-content">
							<div class="tab-pane active" id="m_user_profile_tab_1">
								<form class="m-form m-form--fit m-form--label-align-right">
									<div class="m-portlet__body">
										<div class="form-group m-form__group m--margin-top-10 m--hide">
											<div class="alert m-alert m-alert--default" role="alert">
												The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
											</div>
										</div>
									
										
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">first Name</label>
											<div class="col-7">
											<input disabled class="form-control m-input" type="text" value="{{Auth::user()->F_name}}">
											</div>
										</div>
										
										<div class="form-group m-form__group row">
												<label for="example-text-input" class="col-2 col-form-label">last Name</label>
												<div class="col-7">
												<input disabled class="form-control m-input" type="text" value="{{Auth::user()->L_name}}">
												</div>
											</div>
											

										<div class="form-group m-form__group row">
												<label for="example-text-input" class="col-2 col-form-label">level</label>
												<div class="col-7">
												<input disabled class="form-control m-input" type="text" value="{{Auth::user()->level}}">
												</div>
											</div>
											
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<div class="row">
												<div class="col-2">
												</div>
												<div class="col-7">
													<button disabled type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Save changes</button>&nbsp;&nbsp;
												<a href="{{route('admin.senders.index')}}"  class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</a>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane " id="m_user_profile_tab_2">
							</div>
							<div class="tab-pane " id="m_user_profile_tab_3">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

</div>








@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/metronic-datatable/base/data-local.js')}}" type="text/javascript"></script>





@endsection