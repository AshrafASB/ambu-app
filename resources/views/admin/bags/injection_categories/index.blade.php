@extends('base_layout.master_layout')
@section('title','injection_categories')

@section('style')
    <style>
    </style>
@endsection

@section('body')



    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <!-- END: Subheader -->
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">


                <div class="m-portlet__body">

                      
                    
                    	<!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-md-8">
                       
                        <form action="{{route('admin.bags.injection_categories.index')}}" method="get">
                            <div class="row">
                            <div class="col-md-5">
                                    <div class="m-input-icon m-input-icon--left">
                                          
                                            <input 
                                            autofocus
                                                     name="name"
                                                      value="{{$name}}"
                                                       type="text"
                                                        class="form-control m-input m-input--solid"
                                                         placeholder="Search by serial..."
                                                          id="generalSearch">

                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>

                            </div>    
                            <div class="col-md-3">
                                
											<button type="submit" class="btn btn-primary">search</button>		
                                 
                                </div>    
                            
                            </div>    
                           
                            
                                    
                        </form>
                        
                     
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <a href="{{route('admin.bags.injection_categories.create')}}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                            <i class="la la-cart-plus"></i>
                            <span>New injection category</span>
                            
                        </span>
                    </a>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div>
        </div>

        <!--end: Search Form -->
           


                    <!--begin: Datatable -->

                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data"
                         style="">
                        <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                            <thead class="m-datatable__head">
                            <tr class="m-datatable__row" style="left: 0px;">
                                <th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
                                    <span style="width: 110px;">#</span>
                                </th>

                                <th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
                                    <span style="width: 110px;">Name</span>
                                </th>


                                <th data-field="Type" class="m-datatable__cell m-datatable__cell--sort">
                                    <span style="width: 110px;">created_at</span>
                                </th>
                                <th data-field="Actions" class="m-datatable__cell m-datatable__cell--sort">
                                    <span style="width: 110px;">Actions</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody class="m-datatable__body" style="">


                            @foreach($injection_categories as $injection_categorie)
                                <tr data-row="0" class="m-datatable__row" style="left: 0px;">


                                    <td data-field="ShipName" class="m-datatable__cell">
                                        <span style="width: 110px;"> {{$injection_categorie->id}} </span>
                                    </td>


                                    <td data-field="Currency" class="m-datatable__cell">
                                        <span style="width: 100px;">{{$injection_categorie->name}}</span>
                                    </td>


                                    <td data-field="Latitude" class="m-datatable__cell"><span
                                                style="width: 110px;">{{$injection_categorie->created_at->diffForHumans()}}</span>
                                    </td>


                                    <td data-field="Actions" class="m-datatable__cell">
									<span style="overflow: visible; position: relative; width: 110px;">
										<div class="dropdown ">
											<a href="#"
                                               class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                               data-toggle="dropdown">
												<i class="la la-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
										<!-- <button type="submit" class="btn btn-link">Delete</button> -->


										<a class="dropdown-item"
                                           href="{{ route('admin.bags.injection_categories.edit',$injection_categorie->id) }}">
													<i class="la la-edit"></i> Edit</a>


		
																										
													<a class="dropdown-item delete-btn"
                                                       data-id='{{$injection_categorie->id}}'
                                                       href="#">
										<i class="la la-trash"></i> Delete
								</a>



												
											</div>
										</div>
										<a class='view-ambulance' data-id="{{ 5 }}" data-toggle="modal"
                                           data-target="#m_modal_ambulance" href="#"
                                           class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                           title="View ">
											<i class="la la-edit"></i>
										</a>


									</span>
                                    </td>
                                </tr>

                            @endforeach


                            <tbody>

                        </table>
                    </div>


                    <!--end: Datatable -->


                </div>
            </div>
        </div>
    </div>




    <!--begin::Modal-->
    <div class="modal fade" id="m_modal_ambulance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-form__heading">
                                <h3 class="m-form__heading-title">Ambulance Info:</h3>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="name" class="col-lg-2 col-form-label">Name:</label>
                                <div class="col-lg-6">
                                    <input disabled disabled id="Ambulance-Name" name="name" type="text"
                                           class="form-control m-input">
                                </div>
                            </div>


                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button  data-dismiss="modal" type="button" class="btn btn-primary">Send message</button> -->
                </div>
            </div>
        </div>
    </div>

    <!--end::Modal-->





@endsection

@section('script')
    <script src="{{asset('default/assets/demo/default/custom/crud/metronic-datatable/base/data-local.js')}}"
            type="text/javascript"></script>




    <script>
        var Ambulance_name = document.getElementById('Ambulance-Name');
        var user = document.querySelector('.view-ambulance');
        var modal = document.querySelector('#m_modal_ambulance');
        $('.view-ambulance').click(function (e) {
            modal.display = 'none';
            e.preventDefault();
            var id = $(this).data('id');
            console.log(id);


            $.get('{{ route('admin.ambulances.show')}}', {
                    ambulance_id: id,
                },
                function (data) {
                    Ambulance_name.value = data.ambulance_data.name;
                    modal.display = 'block';

                });


        });


        $('.delete-btn').click(function (e) {
            e.preventDefault();

            var id = $(this).data('id');

            $.get('{{route('admin.bags.injection_categories.delete')}}', {
                injection_category_id: id
            }, function (data) {
                console.log(data.success)
                window.location.reload();
            });

        });


    </script>

@endsection
