@extends('base_layout.master_layout')
@section('title','create part')
@section('style')
<style>

</style>
@endsection

@section('body')


<div class="row">
  <div class="col-lg-12">

    <!--begin::Portlet-->
    <div class="m-portlet">


      <!--begin::Form-->
      <form method="post" action="{{route('admin.body.chest_abdomen_infections.store')}}" class="m-form m-form--label-align-right">
        @csrf
        <div class="m-portlet__body">
          <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
              <h3 class="m-form__heading-title">New part:</h3>
            </div>

            <div class="form-group m-form__group row">
              <label for="name" class="col-lg-2 col-form-label">Name:</label>
              <div class="col-lg-6">
                <input value="{{old('name')}}" id="name" name="name" type="text" class="form-control m-input" placeholder="Enter name" autofocus>
                <span class="m-form__help red"> {{$errors->first('name')}} </span>


              </div>
            </div>



            <div class="form-group m-form__group row">
                <label for="chest_abdomen" class="col-lg-2 col-form-label">body:</label>
  
                <div class="col-lg-6">
         
                    <select 
                    class="form-control m-input m-input--square"
                     id="chest_abdomen"
                      name="chest_abdomen">
  
                        <option value="-1" disabled selected>choose body</option>
                        @foreach ($body as $item)
                    <option  value="{{$item->id}}" {{old('chest_abdomen')== $item->id? 'selected':''}} >{{$item->name}}</option>
                            
                        @endforeach
                      </select>
  
                  <span class="m-form__help red"> {{$errors->first('chest_abdomen')}} </span>
  
  
                </div>
              </div>











          </div>


        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions">
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{route('admin.body.chest_abdomen_infections.index')}}" class="btn btn-secondary">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </form>

      <!--end::Form-->
    </div>

  </div>
</div>

@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>

@endsection
