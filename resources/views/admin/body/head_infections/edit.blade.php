@extends('base_layout.master_layout')
@section('title','edit part')
@section('style')
<style>

</style>
@endsection

@section('body')


<div class="row">
  <div class="col-lg-12">

    <!--begin::Portlet-->
    <div class="m-portlet">


      <!--begin::Form-->
      <form method="post" action="{{route('admin.body.head_infections.update',$head_infections->id)}}" class="m-form m-form--label-align-right">
        @csrf
          @method('PUT')
        <div class="m-portlet__body">
          <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
              <h3 class="m-form__heading-title">edit head_infection:</h3>
            </div>

            <div class="form-group m-form__group row">
              <label for="name" class="col-lg-2 col-form-label">Name:</label>
              <div class="col-lg-6">
                <input value="{{old('name',$head_infections->name)}}" 
                id="name"
                 name="name"
                  type="text"
                   class="form-control m-input"
                    placeholder="Enter name"
                     autofocus>
                <span class="m-form__help red"> {{$errors->first('name')}} </span>


              </div>
            </div>

            <div class="form-group m-form__group row">
                <label for="head_id" class="col-lg-2 col-form-label">body:</label>
  
                <div class="col-lg-6">
         
                    <select 
                    class="form-control m-input m-input--square"
                     id="head_id"
                      name="head_id">
  
                        <option value="-1" disabled selected>choose body</option>
                        @foreach ($body as $item)
                    <option value="{{$item->id}}" {{$item->id == $head_infections->head_id ? 'selected': ''}} >{{$item->name}}</option>
                            
                        @endforeach
                      </select>
  
                  <span class="m-form__help red"> {{$errors->first('head_id')}} </span>
  
  
                </div>
              </div>

              










          </div>


        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions">
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{route('admin.body.head_infections.index')}}" class="btn btn-secondary">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </form>

      <!--end::Form-->
    </div>

  </div>
</div>

@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>

@endsection
