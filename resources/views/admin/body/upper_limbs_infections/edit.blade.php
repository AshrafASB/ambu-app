@extends('base_layout.master_layout')
@section('title','eidt upper_limbs_infections')
@section('style')
<style>

</style>
@endsection

@section('body')


<div class="row">
  <div class="col-lg-12">

    <!--begin::Portlet-->
    <div class="m-portlet">


      <!--begin::Form-->
      <form method="post" action="{{route('admin.body.upper_limbs_infections.update',$upper_limbs_infections->id)}}" class="m-form m-form--label-align-right">
        @csrf
        @method('put')
        <div class="m-portlet__body">
          <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
              <h3 class="m-form__heading-title">New upper_limbs_infections:</h3>
            </div>

            <div class="form-group m-form__group row">
              <label for="name" class="col-lg-2 col-form-label">Name:</label>
              <div class="col-lg-6">
                <input value="{{old('name',$upper_limbs_infections->name)}}" id="name" name="name" type="text" class="form-control m-input" placeholder="Enter name" autofocus>
                <span class="m-form__help red"> {{$errors->first('name')}} </span>


              </div>
            </div>




            <div class="form-group m-form__group row">
                <label for="upper_limbs_infections_id" class="col-lg-2 col-form-label">body:</label>
  
                <div class="col-lg-6">
         
                    <select 
                    class="form-control m-input m-input--square"
                     id="upper_limbs_infections_id"
                      name="upper_limbs_infections_id">
  
                        <option value="-1" disabled selected>choose body</option>
                        @foreach ($body as $item)
                    <option value="{{$item->id}}" {{$item->id == $upper_limbs_infections->upper_limbs_id ? 'selected': ''}}>{{$item->name}}</option>
                            
                        @endforeach
                      </select>
  
                  <span class="m-form__help red"> {{$errors->first('upper_limbs_infections_id')}} </span>
  
  
                </div>
              </div>











          </div>


        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions">
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{route('admin.body.upper_limbs_infections.index')}}" class="btn btn-secondary">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </form>

      <!--end::Form-->
    </div>

  </div>
</div>

@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>

@endsection
