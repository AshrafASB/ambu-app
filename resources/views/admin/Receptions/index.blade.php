@extends('base_layout.master_layout')
@section('title','Receptions')

@section('style')
    <style>

    </style>
@endsection

@section('body')



    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <!-- END: Subheader -->
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">


                <div class="m-portlet__body">
	<!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-md-8">
                       
                        <form action="{{route('admin.Receptions.index')}}" method="get">
                            <div class="row">
                            <div class="col-md-5">
                                    <div class="m-input-icon m-input-icon--left">
                                          
                                            <input 
                                            autofocus
                                                     name="F_name"
                                                      value="{{$F_name}}"
                                                       type="text"
                                                        class="form-control m-input m-input--solid"
                                                         placeholder="Search by serial..."
                                                          id="generalSearch">

                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>

                            </div>    
                            <div class="col-md-3">
                                
											<button type="submit" class="btn btn-primary">search</button>		
                                 
                                </div>    
                            
                            </div>    
                           
                            
                                    
                        </form>
                        
                     
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <a href="{{route('admin.Receptions.create')}}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                            <i class="la la-cart-plus"></i>
                            <span>New Reception</span>
                        </span>
                    </a>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div>
        </div>

        <!--end: Search Form -->
           

                    <!--begin: Datatable -->

                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data"
                         style="">
                        <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                            <thead class="m-datatable__head">
                            <tr class="m-datatable__row" style="left: 0px;">
                                <th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
                                    <span style="width: 110px;">First Name</span>
                                </th>
                                <th data-field="Currency" class="m-datatable__cell m-datatable__cell--sort">
                                    <span style="width: 100px;">Last Name</span>
                                </th>
                                <th data-field="ShipAddress" class="m-datatable__cell m-datatable__cell--sort">
                                    <span style="width: 110px;">username</span>
                                </th>
                                <th data-field="ShipDate" class="m-datatable__cell m-datatable__cell--sort"><span
                                            style="width: 110px;">email</span></th>
                                {{-- <th data-field="Latitude" class="m-datatable__cell m-datatable__cell--sort"><span
                                            style="width: 110px;">Birthday</span></th> --}}

                                <th data-field="Type" class="m-datatable__cell m-datatable__cell--sort"><span
                                            style="width: 110px;">created_at</span></th>
                                <th data-field="Actions" class="m-datatable__cell m-datatable__cell--sort"><span
                                            style="width: 110px;">Actions</span></th>
                            </tr>
                            </thead>

                            <tbody class="m-datatable__body" style="">


                            @foreach($resps as $resp)
                                <tr data-row="0" class="m-datatable__row" style="left: 0px;">


                                    <td data-field="ShipName" class="m-datatable__cell">
                                        <span style="width: 110px;"> {{$resp->F_name}} </span>
                                    </td>


                                    <td data-field="Currency" class="m-datatable__cell">
                                        <span style="width: 100px;">{{$resp->L_name}}</span>
                                    </td>


                                    <td data-field="ShipAddress" class="m-datatable__cell">
                                        <span style="width: 110px;">{{$resp->userName}}</span>
                                    </td>


                                    <td data-field="ShipDate" class="m-datatable__cell">
                                        <span style="width: 110px;">{{$resp->email}}</span>
                                    </td>


                                    {{-- <td data-field="Latitude" class="m-datatable__cell"><span
                                                style="width: 110px;">{{$resp->Birthday}}</span>
                                    </td> --}}


                                    <td data-field="Latitude" class="m-datatable__cell"><span
                                                style="width: 110px;">{{$resp->created_at->diffForHumans()}}</span>
                                    </td>


                                    <td data-field="Actions" class="m-datatable__cell">
									<span style="overflow: visible; position: relative; width: 110px;">
										<div class="dropdown ">
											<a href="#"
                                               class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                               data-toggle="dropdown">
												<i class="la la-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
										<!-- <button type="submit" class="btn btn-link">Delete</button> -->

										
										<a class="dropdown-item"
                                           href="{{ route('admin.Receptions.edit', [$resp->id]) }}">
													<i class="la la-edit"></i> Edit</a>

							
													
								
			
													
								<a class="dropdown-item delete-btn"
                                   data-id='{{$resp->id}}'
                                   href="#">
										<i class="la la-trash"></i> Delete
								</a>

							
													

								
											
							

											</div>
										</div>
										<a class='view-user' data-id="{{ $resp->id }}" data-toggle="modal"
                                           data-target="#m_modal_4" href="#"
                                           class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                           title="View ">
											<i class="la la-edit"></i>
										</a>
										
										
									</span>
                                    </td>
                                </tr>

                            @endforeach


                            <tbody>

                        </table>

                    </div>


                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>




    <!--begin::Modal-->
    <div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-form__heading">
                                <h3 class="m-form__heading-title">Reception Info:</h3>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="first-name" class="col-lg-2 col-form-label">first Name:</label>
                                <div class="col-lg-6">
                                    <input disabled disabled id="first-name" name="firstName" type="text"
                                           class="form-control m-input" placeholder="Enter first name" autofocus>
                                    <span class="m-form__help red"> {{$errors->first('firstName')}} </span>


                                </div>
                            </div>


                            <div class="form-group m-form__group row">
                                <label for="last-name" class="col-lg-2 col-form-label">last Name:</label>
                                <div class="col-lg-6">
                                    <input disabled name="lastName" id="last-name" type="text"
                                           class="form-control m-input" placeholder="Enter last name">

                                    <span class="m-form__help"> {{$errors->first('lastName')}} </span>

                                </div>
                            </div>


                            <div class="form-group m-form__group row">
                                <label for="birthdate" class="col-lg-2 col-form-label">Birthdate:</label>
                                <div class="col-lg-6">

                                    <div class="input-group date">
                                        <input disabled id="birthdate" name="Birthdate" type="text"
                                               class="form-control m-input" readonly="" placeholder="Select date"
                                               id="m_datepicker_2">
                                        <div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-calendar-check-o"></i>
											</span>
                                        </div>


                                        <span class="m-form__help"> {{$errors->first('Birthdate')}} </span>


                                    </div>
                                </div>
                            </div>


                            <div class="form-group m-form__group row">
                                <label for="email" class="col-lg-2 col-form-label">Email address:</label>
                                <div class="col-lg-6">
                                    <input disabled name="email" id="email" type="email" class="form-control m-input"
                                           placeholder="Enter email">

                                    <span class="m-form__help"> {{$errors->first('email')}} </span>

                                </div>
                            </div>


                            <div class="form-group m-form__group row">
                                <label for="username" class="col-lg-2 col-form-label">username:</label>
                                <div class="col-lg-6">
                                    <input disabled name="username" id="username" type="text"
                                           class="form-control m-input" placeholder="Enter username">

                                    <span class="m-form__help"> {{$errors->first('username')}} </span>

                                </div>
                            </div>


                            <div class="form-group m-form__group row">
                                <label for="level" class="col-lg-2 col-form-label">level:</label>
                                <div class="col-lg-6">
                                    <input disabled name="level" id="level" type="text" class="form-control m-input"
                                           placeholder="Enter level">

                                    <span class="m-form__help"> {{$errors->first('level')}} </span>

                                </div>
                            </div>


                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button  data-dismiss="modal" type="button" class="btn btn-primary">Send message</button> -->
                </div>
            </div>
        </div>
    </div>

    <!--end::Modal-->




@endsection

@section('script')
    <script src="{{asset('default/assets/demo/default/custom/crud/metronic-datatable/base/data-local.js')}}"
            type="text/javascript"></script>




    <script>
        var firstName = document.getElementById('first-name');
        var lastName = document.getElementById('last-name');
        var birthdate = document.getElementById('birthdate');
        var email = document.getElementById('email');
        var username = document.getElementById('username');
        var level = document.getElementById('level');
        var user = document.querySelector('.view-user');
        var modal = document.querySelector('#m_modal_4');


        $('.view-user').click(function (e) {
            modal.display = 'none';
            e.preventDefault();
            var id = $(this).data('id');
            console.log(id);


            $.get('{{ route('admin.Receptions.show')}}', {
                    resp_id: id,
                },
                function (data) {
                    firstName.value = data.resp_data.F_name
                    lastName.value = data.resp_data.L_name
                    birthdate.value = data.resp_data.Birthday
                    email.value = data.resp_data.email
                    username.value = data.resp_data.userName
                    level.value = data.resp_data.level
                    modal.display = 'block';

                    user.setAttribute('data-target', '#m_modal_4')
                    console.log('succ');
                });


        });

        $('.delete-btn').click(function (e) {
            e.preventDefault();

            var id = $(this).data('id');

            $.get('{{route('admin.Receptions.delete')}}', {
                resp_id: id
            }, function (data) {
                window.location.reload();
                console.log(data.success)
            });

        });

    </script>

@endsection