@extends('base_layout.master_layout')
@section('title','Edit Reception')

@section('style')
<style>

</style>
@endsection

@section('body')


<div class="row">
  <div class="col-lg-12">

    <!--begin::Portlet-->
    <div class="m-portlet">


      <!--begin::Form-->
      <form method="post" action="{{route('admin.Receptions.update',$user->id)}}" class="m-form m-form--label-align-right">
        @csrf
        @method('PUT')

        <div class="m-portlet__body">
          <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
              <h3 class="m-form__heading-title">Customer Info:</h3>
            </div>

            <div class="form-group m-form__group row">
              <label for="first-name" class="col-lg-2 col-form-label">first Name:</label>
              <div class="col-lg-6">
                <input value="{{old('firstName',$user->F_name)}}" id="first-name" name="firstName" type="text" class="form-control m-input" placeholder="Enter first name" autofocus>
                <span class="m-form__help red"> {{$errors->first('firstName')}} </span>


              </div>
            </div>





            <div class="form-group m-form__group row">
              <label for="last-name" class="col-lg-2 col-form-label">last Name:</label>
              <div class="col-lg-6">
                <input value="{{old('lastName',$user->L_name)}}" name="lastName" id="last-name" type="text" class="form-control m-input" placeholder="Enter last name">

                <span class="m-form__help"> {{$errors->first('lastName')}} </span>

              </div>
            </div>



            <div class="form-group m-form__group row">
              <label for="first-name" class="col-lg-2 col-form-label">Birthdate:</label>
              <div class="col-lg-6">

                <div class="input-group date">
                  <input value="{{old('Birthdate',$newDate)}}" name="Birthdate" type="text" class="form-control m-input" readonly="" placeholder="Select date" id="m_datepicker_2">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="la la-calendar-check-o"></i>
                    </span>
                  </div>


                  <span class="m-form__help"> {{$errors->first('Birthdate')}} </span>


                </div>
              </div>
            </div>



            <div class="form-group m-form__group row">
              <label for="email" class="col-lg-2 col-form-label">Email address:</label>
              <div class="col-lg-6">
                <input value="{{old('email',$user->email)}}" name="email" id="email" type="email" class="form-control m-input" placeholder="Enter email">

                <span class="m-form__help"> {{$errors->first('email')}} </span>

              </div>
            </div>

            

            <div class="form-group m-form__group row">
              <label for="username" class="col-lg-2 col-form-label">username:</label>
              <div class="col-lg-6">
                <input value="{{old('username',$user->userName)}}" name="username" id="username" type="text" class="form-control m-input" placeholder="Enter username">

                <span class="m-form__help"> {{$errors->first('username')}} </span>

              </div>
            </div>




            <div class="form-group m-form__group row">
              <label for="password" class="col-lg-2 col-form-label">Password:</label>
              <div class="col-lg-6">
                <input  name="password" id="password" type="password" class="form-control m-input" placeholder="Enter Password">
                <span class="m-form__help"> {{$errors->first('password')}} </span>

              </div>
            </div>


        
            
          </div>


        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions">
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{route('admin.Receptions.index')}}" class="btn btn-secondary">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </form>

      <!--end::Form-->
    </div>

  </div>
</div>

@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>

@endsection