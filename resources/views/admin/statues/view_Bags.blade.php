@extends('base_layout.master_layout')
@section('title','statuses')

@section('style')
<style>
</style>
@endsection

@section('body')


	
<div class="row">
		@if ($items)
	
		
		@foreach($items as $key =>$item)
		@if($loop->iteration == 3)
		@break
		@endif
				<div class="col-xl-6">
												
					<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											{{$key}}
										</h3>
									</div>
								</div>
							</div>
							<div class="m-portlet__body">
					
								<!--begin::Section-->
								<div class="m-section">
									<div class="m-section__content">
										<table class="table table-bordered m-table m-table--border-success">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
												</tr>
											</thead>
											<tbody>
												 @foreach ($item as $case)
												 <tr>
														<th scope="row">{{$loop->iteration}}</th>
														<td>{{$case}}</td>
													</tr>
												 @endforeach
												 
											</tbody>
										</table>
									</div>
								</div>
					
								<!--end::Section-->
								
					
								<!--end::Section-->
							</div>
					
							<!--end::Form-->
						</div>
					
								</div>
				@endforeach
			
	

				
		@endif
			<!--end::Form-->
		</div>




@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/metronic-datatable/base/data-local.js')}}" type="text/javascript"></script>
</script>
@endsection