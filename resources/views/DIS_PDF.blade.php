<!DOCTYPE>
<html>
<head>
    <title>{{ $title }}</title>
    <style>
        body {
            width: 100%;
            height: 2000px;
            margin: 10px;
            padding: 20px;
            font-weight: bold;
            border: 10px solid black;
        }
        .aa{
            border-right: 3px solid black;
            height: 300px;
        }

    </style>
</head>
<body>

<div>
    <div>
        <div>
            <!--begin::Section-->
            <table border="0.5px" style="width: 70%" >
                <thead>
                <tr>
                    <th>Body</th>

                    <th>Bag</th>
                </tr>

                </thead>
                <tbody>
                <tr>

                    <td>
                        <table style="width: 100%">

                            <tr>
                                <td>
                                    <table  class="aa" style="width:100%" >
                                        @if ($body1)
                                            @foreach($body1 as $key =>$item)
                                                @if($loop->iteration == 3)

                                                    @break
                                                @endif

                                                <tr> <td><hr>{{$key}}<hr></td>

                                                </tr>

                                                {{--<tr>--}}
                                                {{--<td>#</td>--}}
                                                {{--<td>Name</td>--}}
                                                {{--</tr>--}}

                                                @foreach ($item as $case)
                                                    <tr>
                                                        {{--<td>{{$loop->iteration}}</td>--}}
                                                        <td>{{$case}}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </table>
                                </td>

                                <td>
                                    <table style="width: 50%">
                                        @if ($body2)
                                            @foreach($body2 as $key =>$item)
                                                @if($loop->iteration == 3)
                                                    @break
                                                @endif
                                                <tr><td><hr>{{$key}}<hr></td></tr>
                                                @foreach ($item as $case)
                                                    <tr>
                                                        {{--<td>{{$loop->iteration}}</td>--}}
                                                        <td>{{$case}}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>

                    <td>
                        <table style="width:50%">
                            @if ($bag)
                                @foreach($bag as $key =>$item)
                                    @if($loop->iteration == 3)
                                        @break
                                    @endif
                                    <tr><td><hr>{{$key}}<hr></td></tr>
                                    @foreach ($item as $case)
                                        <tr>
                                            {{--                                               <td>{{$loop->iteration}}</td>--}}
                                            <td>{{$case}}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif

                        </table>
                    </td>

                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


</body>
</html>
