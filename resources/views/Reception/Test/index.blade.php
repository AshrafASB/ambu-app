@extends('base_layout.master_layout')
@section('title','statuses')

@section('style')
<style>
</style>
@endsection

@section('body')


			
		<!-- END: Left Aside -->
		<div class="m-grid__item m-grid__item--fluid m-wrapper">


		

<!-- END: Subheader -->
<div class="m-content">
	
	<div class="m-portlet m-portlet--mobile">
	
	
		<div class="m-portlet__body">

			<!--begin: Search Form -->
			<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="form-group m-form__group row align-items-center">
							<div class="col-md-4">
								<div class="m-form__group m-form__group--inline">
									<div class="m-form__label">
										<label>Status:</label>
									</div>
									<div class="m-form__control">
										<select class="form-control m-bootstrap-select" id="m_form_status">
											<option value="">All</option>
											<option value="1">Pending</option>
											<option value="2">Delivered</option>
											<option value="3">Canceled</option>
										</select>
									</div>
								</div>
								<div class="d-md-none m--margin-bottom-10"></div>
							</div>
							<div class="col-md-4">
								<div class="m-form__group m-form__group--inline">
									<div class="m-form__label">
										<label class="m-label m-label--single">Type:</label>
									</div>
									<div class="m-form__control">
										<select class="form-control m-bootstrap-select" id="m_form_type">
											<option value="">All</option>
											<option value="1">Online</option>
											<option value="2">Retail</option>
											<option value="3">Direct</option>
										</select>
									</div>
								</div>
								<div class="d-md-none m--margin-bottom-10"></div>
							</div>
							<div class="col-md-4">
								<div class="m-input-icon m-input-icon--left">
									<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
									<span class="m-input-icon__icon m-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				
					
				</div>
			</div>

			<!--end: Search Form -->

			
			
				<!--begin: Datatable -->

				<div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" style="">
					<table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
						<thead class="m-datatable__head">
							<tr class="m-datatable__row" style="left: 0px;">
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">#</span>
								</th>
								
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Name</span>
								</th>
					
								
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Gender</span>
								</th>
						
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">arrived</span>
								</th>
						
						
								<!-- <th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">age_id</span>
								</th> -->
						
								
								<th data-field="Type" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">created_at</span>
								</th>
								<th data-field="Actions" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Actions</span>
								</th>
							</tr>
						</thead>

						<tbody class="m-datatable__body" style="">



                            


						<tbody>

					</table>
					<div class="m-datatable__pager m-datatable--paging-loaded clearfix">
						<ul class="m-datatable__pager-nav">
							<li><a title="First" class="m-datatable__pager-link m-datatable__pager-link--first" data-page="1"><i class="la la-angle-double-left"></i></a></li>
							<li><a title="Previous" class="m-datatable__pager-link m-datatable__pager-link--prev" data-page="1"><i class="la la-angle-left"></i></a></li>
							<li style="display: none;"><a title="More pages" class="m-datatable__pager-link m-datatable__pager-link--more-prev" data-page="1"><i class="la la-ellipsis-h"></i></a></li>
							<li style="display: none;"><input disabled type="text" class="m-pager-input form-control" title="Page number"></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="1" title="1">1</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active" data-page="2" title="2">2</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="3" title="3">3</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="4" title="4">4</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="5" title="5">5</a></li>
							<li style="display: none;"><a title="More pages" class="m-datatable__pager-link m-datatable__pager-link--more-next" data-page="5"><i class="la la-ellipsis-h"></i></a></li>
							<li><a title="Next" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="3"><i class="la la-angle-right"></i></a></li>
							<li><a title="Last" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="5"><i class="la la-angle-double-right"></i></a></li>
						</ul>


					</div>
				</div>


				<!--end: Datatable -->
			

		</div>
	</div>
</div>
</div>








@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/metronic-datatable/base/data-local.js')}}" type="text/javascript"></script>





@endsection