@extends('base_layout.master_layout')
@section('title','statuses')

@section('style')
<style>
</style>
@endsection

@section('body')


			
		<!-- END: Left Aside -->
		<div class="m-grid__item m-grid__item--fluid m-wrapper">


		

<!-- END: Subheader -->
<div class="m-content">
	
	<div class="m-portlet m-portlet--mobile">
	
	
		<div class="m-portlet__body">

			<!--begin: Search Form -->
			<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			{{-- <form action="{{route('admin.statuses.index')}}" method="get">

					<div class="row align-items-center">
							<div class="col-xl-12 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-3">
										<div class="m-form__group m-form__group--inline">
											<div class="m-form__label">
												<label>injured organ:</label>
											</div>
											<div class="m-form__control">
												<select class="form-control m-bootstrap-select" id="m_form_status" name="parentBody">
													<option value="-1">All</option>
													@foreach ($body as $item)
													<option value="{{$item->id}}" {{$parentBody == $item->id? 'selected':''}}>{{$item->name}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="d-md-none m--margin-bottom-10"></div>
									</div>
									<div class="col-md-3">
										<div class="m-form__group m-form__group--inline">
											<div class="m-form__label">
												<label class="m-label m-label--single">Drugs category:</label>
											</div>
											<div class="m-form__control">
												<select class="form-control m-bootstrap-select" id="m_form_type" name="parentBag">
														<option value="-1">All</option>
														@foreach ($bag as $item)
														<option value="{{$item->id}}" {{$parentBag == $item->id? 'selected':''}}>{{$item->name}}</option>
														@endforeach
												</select>
											</div>
										</div>
										<div class="d-md-none m--margin-bottom-10"></div>
									</div>
									<div class="col-md-3">
										<div class="m-input-icon m-input-icon--left">
											<input name="seraiNumber" value="{{$seraiNumber}}" type="text" class="form-control m-input m-input--solid" placeholder="Search by serial..." id="generalSearch">
											<span class="m-input-icon__icon m-input-icon__icon--left">
												<span><i class="la la-search"></i></span>
											</span>
										</div>
									</div>
		
									<div class="col-md-3">
											<button type="submit" class="btn btn-primary">search</button>		
									</div>
		
								</div>
							</div>
						
							
						</div>
			</form> --}}
				
			</div>

			<!--end: Search Form -->

			
			
				<!--begin: Datatable -->

				<div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" style="">
					<table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
						<thead class="m-datatable__head">
							<tr class="m-datatable__row" style="left: 0px;">
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">#</span>
								</th>
								
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">pationt NO</span>
								</th>
					
{{-- 								
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">injured organ
									</span>
								</th>
						

								
								<th data-field="ShipName" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Drugs category</span>
								</th> --}}
						
								
								
								<th data-field="Type" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">created_at</span>
								</th>
								<th data-field="Actions" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Actions</span>
								</th>
							</tr>
						</thead>

						<tbody class="m-datatable__body" style="">



							@foreach($stats as $stat)
							<tr data-row="0" class="m-datatable__row" style="left: 0px;">


								<td data-field="ShipName" class="m-datatable__cell">
									<span style="width: 110px;"> {{$stat->id}} </span>
								</td>
						
								

								<td data-field="Currency" class="m-datatable__cell">
									<span style="width: 100px;">{{$stat->name}}</span>
								</td>


{{-- 
								<td data-field="Currency" class="m-datatable__cell">
									<span style="width: 100px;">{{$stat->parenBody->name}}</span>
								</td>



								<td data-field="Currency" class="m-datatable__cell">
									<span style="width: 100px;">{{$stat->parenBag->name}}</span>
								</td>

 --}}

								


								<td data-field="Latitude" class="m-datatable__cell"><span style="width: 110px;">{{$stat->created_at->diffForHumans()}}</span>
								</td>



								<td data-field="Actions" class="m-datatable__cell">
									<span style="overflow: visible; position: relative; width: 110px;">
									
										
											{{-- href="{{route('Receptions.statuses.view',$stat->id)}}" --}}
										<a class='view-pationt'
										href="#"
class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" 
title="Generate report">
											<i class="la la-print"></i>
										</a>


										<a class='view-pationt'
										href="{{route('reception.statuses.view-injuers',$stat->id)}}"
class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" 
title="injuers">
										<i class="fa fa-ambulance"></i>
									</a>
										<a class='view-pationt'
										   href="{{route('reception.statuses.view-Bags',$stat->id)}}"
class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" 
title="Bags">
										   <i class="fa fa-shopping-bag"></i>
									   </a>

										
										
									</span>
								</td>
							</tr>

							@endforeach





						<tbody>

					</table>
					<div class="m-datatable__pager m-datatable--paging-loaded clearfix">
						<ul class="m-datatable__pager-nav">
							<li><a title="First" class="m-datatable__pager-link m-datatable__pager-link--first" data-page="1"><i class="la la-angle-double-left"></i></a></li>
							<li><a title="Previous" class="m-datatable__pager-link m-datatable__pager-link--prev" data-page="1"><i class="la la-angle-left"></i></a></li>
							<li style="display: none;"><a title="More pages" class="m-datatable__pager-link m-datatable__pager-link--more-prev" data-page="1"><i class="la la-ellipsis-h"></i></a></li>
							<li style="display: none;"><input disabled type="text" class="m-pager-input form-control" title="Page number"></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="1" title="1">1</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active" data-page="2" title="2">2</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="3" title="3">3</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="4" title="4">4</a></li>
							<li><a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="5" title="5">5</a></li>
							<li style="display: none;"><a title="More pages" class="m-datatable__pager-link m-datatable__pager-link--more-next" data-page="5"><i class="la la-ellipsis-h"></i></a></li>
							<li><a title="Next" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="3"><i class="la la-angle-right"></i></a></li>
							<li><a title="Last" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="5"><i class="la la-angle-double-right"></i></a></li>
						</ul>


					</div>
				</div>


				<!--end: Datatable -->
			

		</div>
	</div>
</div>
</div>







@endsection

@section('script')
<script src="{{asset('default/assets/demo/default/custom/crud/metronic-datatable/base/data-local.js')}}" type="text/javascript"></script>







<script>
</script>


@endsection