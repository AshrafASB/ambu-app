<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <title>Ambu_App Report</title>

    <style>
        .head{
            font-size:50px ;
            text-align: center;
            color: red;
            font-style: normal;

        }

        .table{
            width: 75%;
            margin: 50px 155px 50px 155px;
            border-collapse: collapse;
        }

        .table, th, td {
            border: 3px solid black;
        }
        .img {
            border-radius: 50%;
            /*align-items: center;*/
            margin-left: 50%;
        }

        .all{
            margin: 5px;
            border: 10px solid black ;
        }
        td,th{
            text-align: center;
        }


    </style>
</head>
<div class="all">
    <body>
    <br>
    <h1 class="head">Ambu-App</h1>
    <h2 style="font-size: 30px ; color: black; text-align: center">Injury Report</h2>

    <!--1st  table-->
    <table class="table" border="1">
        <tbody >
        <tr class="table-active" >
            <td  width="200px"  >Paramedic Name</td>
            <td colspan="3">{{$content->sender->F_name}}</td>

        </tr>
        <tr>
            <td colspan="0.5">Ambulance Number</td>
            <td width="250px"> {{$content->amblunce->name}}  </td>
            <td>Patient number</td>
            <td> {{$content->name}}   </td>
        </tr>
        <tr>

        </tbody>
    </table>

    <h3 style="margin-left: 150px">Body Injury :-</h3>
    <!--2nd table-->

    <table  class="table table-striped" border="1">
        <tbody >
        <tr class="table-active" >
            <td  width="90px">Date of Injury</td>
            <td colspan="1">{{$content->created_at->format('d/m/Y')}} </td>
            <td  width="90px">Time Of Injury</td>
            <td colspan="1">{{$content->created_at->format('h:i:s a')}}</td>

        </tr>
        <tr>
            <th width="50px">Head Injury</th>
            <th width="50px">Upper Limbs</th>
            <th width="50px">Lower Limbs</th>
            <th width="50px">Chest</th>
        </tr>
        <!--first row -->
        @for($i=1 ; $i<5 ; $i++)
            <tr>
                <td>
                    @if(isset($Head[$i - 1]))
                        {{$i}}- {{$Head[$i - 1]}}
                    @else
                        {{'-------'}}
                    @endif
                </td>

                <td>
                    @if(isset($upper_limbs[$i]))
                        {{$i}}- {{$upper_limbs[$i]}}
                    @else
                        {{'-------'}}
                    @endif
                </td>


                <td>
                    @if(isset($lower_limbs[$i]))
                        {{$i}}- {{$lower_limbs[$i]}}
                    @else
                        {{'-------'}}
                    @endif
                </td>



                <td>
                    @if(isset($chest[$i]))
                        {{$i}}- {{$chest[$i]}}
                    @else
                        {{'-------'}}
                    @endif
                </td>

            </tr>
        @endfor

        </tbody>
    </table>

    <h3 style="margin-left: 150px">Band aid :-</h3>

    <div style="width: 50%">
        <table  class="table rr" border="1">
            <tbody >
            <tr>
                <td  width="20px">influence</td>
                {{--<td colspan="1"></td>--}}
                <td  width="20px">injection</td>
                {{--<td colspan="1"></td>--}}

            </tr>
            @for($i=1 ; $i<5 ; $i++)
                <tr>

                    <td>
                        @if(isset($influence[$i]))
                            {{$i}}- {{$influence[$i]}}
                        @else
                            {{'-------'}}
                        @endif
                    </td>



                    <td>
                        @if(isset($injection[$i]))
                            {{$i}}- {{$injection[$i]}}
                        @else
                            {{'-------'}}
                        @endif
                    </td>



                </tr>
            @endfor

            </tbody>
        </table>
    </div>

    <div class="img">
        <img src="{{asset('homePage/img/footer-img.png')}}" alt="Avatar">
    </div>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>
        window.print();

    </script>
    </body>
</div>
</html>