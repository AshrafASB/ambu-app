<!DOCTYPE html>

<html lang="en">

    <!-- begin::Head -->
 <!-- هذا للروابط السي اس اس -->
		@includeif('base_layout.header.meta_header')
		
    @yield('style')



	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- BEGIN: Header -->
            <!-- هذا الملف يخص الشريط العلوي -->
            @includeif('base_layout.header.header')
			<!-- END: Header -->

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

                <!-- BEGIN: Left Aside -->
                
         <!-- هذا للشريط الموجود ع اليمين -->

            @includeif('base_layout.Navbars.navbar')
			
				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

				
					<div class="m-content">
                        
                    @yield('body')
					</div>
				</div>
			</div>

			<!-- end:: Body -->

            <!-- begin::Footer -->
            @includeif('base_layout.fotter.fotter')

     

			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->

        <!-- begin::Quick Sidebar -->
         <!-- هذا الملف للدردشة الي بتكون ع الجنب -->
         @includeif('base_layout.Navbars.aside_Chatting')

		
		<!-- end::Quick Sidebar -->

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->


        <!--begin::Global Theme Bundle -->
        
         <!-- هذا الملف للروابط تعون ملفات الجافاسكربت -->
    @includeif('base_layout.fotter.meta_fotter')

@yield('script')


		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>