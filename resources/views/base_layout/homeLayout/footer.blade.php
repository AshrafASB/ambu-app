
    <!--  Footer  -->
    <footer id="footer" class="section-bg">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                        <div class="col-lg-4">
                                <img src="img/footer-img.png" alt="">
                            </div>
                    
                    <div class="col-lg-4">
                        <div class="footer-nav">
                            <a href="{{route('FAQ')}}">الأسئلة الشائعة</a>
                            <a href="{{asset('homePage/index.html')}}">عن التطبيق</a>
                            <a href="{{asset('homePage/index.html')}}">الرئيسية</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                            <div class="social-links">
                                <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                                <a href="#" class="facebook"><i class="fab fa-facebook"></i></i></a>
                                <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="copyright">
                Ambu App Team.حقوق الطبع محفوظة </div>
        </div>
    </footer>
    <!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="{{asset('homePage/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('homePage/lib/jquery/jquery-migrate.min.js')}}"></script>
    <script src="{{asset('homePage/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('homePage/lib/easing/easing.min.js')}}"></script>
    <script src="{{asset('homePage/lib/mobile-nav/mobile-nav.js')}}"></script>
    <script src="{{asset('homePage/lib/wow/wow.min.js')}}"></script>
    <script src="{{asset('homePage/lib/waypoints/waypoints.min.js')}}"></script>
    <script src="{{asset('homePage/lib/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('homePage/lib/owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('homePage/lib/isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('homePage/lib/lightbox/js/lightbox.min.js')}}"></script>
    <script src="{{asset('homePage/jssor.slider-27.5.0.min.js')}}"></script>

    <!-- Template Main Javascript File -->
    <script src="{{asset('homePage/js/main.js')}}"></script>

</body>

</html>