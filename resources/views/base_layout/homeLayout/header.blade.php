<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> Ambu App </title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="img/footer-img.png" rel="icon">
    <link href="img/footer-img.png" rel="apple-touch-icon">

    <!-- Bootstrap CSS File -->
    <link href="{{asset('homePage/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{asset('homePage/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('homePage/lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('homePage/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('homePage/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('homePage/lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('homePage/css/bootstrap-rtl.css')}}">
    <script src="https://kit.fontawesome.com/9314e48a0c.js" crossorigin="anonymous"></script>

    <!-- Main Stylesheet File -->
    <link href="{{asset('homePage/css/style.css')}}" rel="stylesheet">

</head>
