@extends('base_layout.homeLayout.master_layout')
@section('title','statuses')

@section('style')
<style>
</style>
@endsection


    <!-- start navbar -->
    <header>
  
        <nav class="navbar navbar-expand-lg navbar-light ">
                <div class="container">
            
            <a class="navbar-brand  " href="{{route('home')}}" >
                <span> <path stroke="black" d="M5 40 l215 0" />Ambu</span>
                <span>App</span>
            
             
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
           
            <div class="collapse navbar-collapse" id="main-nav">
              <ul class="navbar-nav ml-auto" style="margin-right: 190px !important;">
                <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}">الرئيسية  <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#main">عن التطبيق</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#best-features"> مميزاتنا </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="#contactform" >تواصل معنا </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#jssor_1" >شركاؤنا  </a>
                  </li>
                </ul>
              </div>
                <ul style="list-style: none;color:white;">
                <li class="nav-item dropdown dropalign">
                    <a class="nav-link dropdown-toggle welcome" style="color:white;margin-bottom: -15px;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               @auth {{Auth::user()->userName}} @else أهلا وسهلا @endauth
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                     @auth @if(Auth::user()->level==='Admin')   
                    <a class="dropdown-item" href="{{route('admin.senders.index')}}">لوحة الادمن</a>
                    <a class="dropdown-item" href="{{route('home.loguot')}}">تسجيل الخروج</a>
                                @elseif(Auth::user()->level==='reception')

<a class="dropdown-item" href="{{route('reception.statuses.index')}}">لوحة المستقبل</a>
<a class="dropdown-item" href="{{route('home.loguot')}}">تسجيل الخروج</a>
                                @endif
                                @else
<a class="dropdown-item" href="{{route('login')}}">تسجيل الدخول</a>
                                @endauth

                
                    </div>
                  </li>
              </ul>
             
            </div>
          </nav>
        </nav>
        </nav>
    </header>
    <!-- end navbar -->

    
@section('body')


 
        <main id="main" mb-50>
       <div class="container">
            
           <div class="main-container" class="display:flex !important;00flex-direction:row;width:100%">
                <div class="site-content" col-lg-7 col-xl-6 pt-10 >
                        <div  style="width:100%">
                             <div class="d-flex flex-column">
                                 <h1 class="site-title"> تطبيق اسعاف </h1>
                                 <p  class="site-desc"> مهمته هي ربط سيارات الإسعاف بالمستشفى لنقل حالة المصاب 
                                     وما تم إعطائه من علاج لتجهيز أفضل الإعدادات <br>  المناسبة
                                    للحالة و تجنب أي خطأ طبي أو تجنب إعطائه أدوية 
                                     أعطت له في الإسعاف حتى لا يحدث له مضاعفات <br> حيث يتم إرسال البيانات من
                                       iPad الذي مع المسعف إلى الاستقبال في المستشفى
                                 </p>
         
                                <div class="d-flex flex-row">
                                      <input type="button" value="مشاهدة الفيديو" class="btn-primary site-btn1 px-4 py-3 mr-4">
                                 </div>
                          </div>
                        </div>
                       
                    </div>
                
           </div>
        <div>
   </main>
  
        <!--  Our Advantages Section  -->
       
           
                <section id="best-features" class="text-center">
                        <div class="container">
                    <h2 class="title1"> ما يقدمه تطبيقنا من مميزات</h2>
                    <div class="row d-flex justify-content mb-4">
                        
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{asset('homePage/img/video (3).png')}}" alt="">
                            <p class = "mt-3">السهولة في التعامل <br> و المرونة في الاستخدام</p>
                            

                        </div>
                        

                          <div class="col-md-3">
                                        <img src="{{asset('homePage/img/responsive.png')}}" alt="">
                                        <p class = "mt-3">الواجهات الجذابة و الواضحة <br>
                                                للمسعفين للتعامل<br>
                                                 مع التطبيق  </p>
        
                                </div>


                                   
                                        <div class="col-md-3">
                                                <img src="{{asset('homePage/img/e.png')}}" alt="">
                                                <p class = "mt-3">مجسمات الموجودة فيه 
                                                      <br>  التي تسهل اختيار منطقة<br> 
                                                         إصابة المريض</p>
                                        </div>
                                        
                                             <div class="col-md-3">
                                                <img src="{{asset('homePage/img/sand-clock.png')}}" alt="">
                                                  <p class = "mt-3"> أستخدام مبدأ في الوقت 
                                                        الحالي (real time) <br>
                                                       في إرسال الإشعارات
                                                       
                                                  </p>
                                              </div>
                              </div>
                         </div>   
                </section>
                
                            

            
            <br>
            <br>
            <br>
            <br>
            <script src="{{asset('homePage/js/jssor.slider-27.5.0.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 0,
              $SlideDuration: 5000,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Align: 0
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }
    </style>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:100px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:100px;overflow:hidden;">
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/1.jpeg')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/r.png')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/d.png')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/1.jpeg')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/r.png')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/d.png')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/1.jpeg')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/r.png')}}" />
            </div>
            <div>
              
                <img data-u="image" src="{{asset('homePage/img/clients/d.png')}}" />
            </div>
            <div>
                <img data-u="image" src="{{asset('homePage/img/clients/r.png')}}" />
            </div>
           
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->

                <br>
            
                
        <!--  Form Section  -->
        <section id="contactform" >
            <div class="container">
                <div class="row feature-item">
                    
                    <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
                        <h4>تواصل معنا</h4>
                        <div class="form">
                            <h4>ارسل رسالة الان</h4>
                            <form action="" method="post" role="form" class="contactForm" dir="rtl">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="الاسم" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="الإيميل" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="الرسالة"></textarea>
                                    <div class="validation"></div>
                                </div>
                                <button class="btn btn-primary" type="submit" title="Send Message">ارسال</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 wow fadeInUp">
                            <img src="{{asset('homePage/img/Form-img.png')}}" class="img-fluid" alt="">
                        </div>
                </div>
            </div>
        </section>
        <!-- #about -->

    </main>



    @endsection

    @section('script')
    
    
    <script>
    </script>
    
    
    @endsection